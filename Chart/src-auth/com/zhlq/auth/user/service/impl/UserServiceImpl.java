package com.zhlq.auth.user.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jxl.read.biff.BiffException;

import com.zhlq.auth.user.bean.User;
import com.zhlq.auth.user.bean.UserForm;
import com.zhlq.auth.user.service.UserService;
import com.zhlq.condition.Query;
import com.zhlq.condition.Where;
import com.zhlq.core.service.impl.ProjectServiceImpl;
import com.zhlq.export.bean.Cell;
import com.zhlq.export.bean.Data;
import com.zhlq.export.bean.Header;
import com.zhlq.export.bean.Row;
import com.zhlq.util.ExcelUtil;

public class UserServiceImpl extends ProjectServiceImpl implements UserService {

	@Override
	public Header userToHeader(UserForm user) {
		Header header = new Header();
		header.add("主键");
		header.add("名称");
		header.add("密码");
		header.add("状态");
		return header;
	}

	@Override
	public Data userToData(UserForm userForm) {
		Data data = new Data();
		Where where = new Where(User.class);
		where.addIns("id", userForm.getIds());
		List<Object> list = super.retrieve(where);
		if(null==list || list.isEmpty()){
			// 数据为空
			return data;
		}
		
		User user;
		Row row;
		Cell cell;
		int col;
		for(int i = 0; i < list.size(); i++){
			// 遍历所有数据
			col = 0;
			// 将用户转换为行
			user = (User) list.get(i);
			row = new Row();
			cell = new Cell(col++, i, String.valueOf(null==user.getId()?"":user.getId()));
			row.add(cell);
			cell = new Cell(col++, i, String.valueOf(null==user.getName()?"":user.getName()));
			row.add(cell);
			cell = new Cell(col++, i, String.valueOf(null==user.getPassword()?"":user.getPassword()));
			row.add(cell);
			cell = new Cell(col++, i, String.valueOf(null==user.getState()?"":user.getState()));
			row.add(cell);
			data.add(row);
		}
		return data;
	}

	@Override
	public Data listToData(UserForm userForm) {
		Data data = new Data();
		List<Object> list = super.retrieve(new Query(userForm));
		if(null==list || list.isEmpty()){
			// 数据为空
			return data;
		}
		
		User user;
		Row row;
		Cell cell;
		int col;
		for(int i = 0; i < list.size(); i++){
			// 遍历所有数据
			col = 0;
			// 将用户转换为行
			user = (User) list.get(i);
			row = new Row();
			cell = new Cell(col++, i, String.valueOf(null==user.getId()?"":user.getId()));
			row.add(cell);
			cell = new Cell(col++, i, String.valueOf(null==user.getName()?"":user.getName()));
			row.add(cell);
			cell = new Cell(col++, i, String.valueOf(null==user.getPassword()?"":user.getPassword()));
			row.add(cell);
			cell = new Cell(col++, i, String.valueOf(null==user.getState()?"":user.getState()));
			row.add(cell);
			data.add(row);
		}
		return data;
	}

	@Override
	public List<User> fileToUser(File file) {
		List<User> result = new ArrayList<User>();
		int sheetIndex = 0, header = 1, footer = 0;
		int rows;
		try {
			rows = ExcelUtil.getDataNum(file, sheetIndex, header, footer);
		} catch (BiffException | IOException e) {
			throw new RuntimeException(e);
		}//实际行数（不计算表头）
		List<String> list;
		User user;
		for(int i = 0; i < rows; i++){
			try {
				list = ExcelUtil.readLine(file, 0, i + header);
			} catch (BiffException | IOException e) {
				throw new RuntimeException(e);
			}
			user = new User();
			user.setId(null==list.get(0)||"".equals(list.get(0))?null:Integer.parseInt(list.get(0)));
			user.setName(list.get(1));
			user.setPassword(list.get(2));
			user.setState(list.get(3));
			result.add(user);
		}
		return result;
	}

}
