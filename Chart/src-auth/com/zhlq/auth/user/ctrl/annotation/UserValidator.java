package com.zhlq.auth.user.ctrl.annotation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.zhlq.auth.user.bean.User;
import com.zhlq.validator.bean.Validator;
import com.zhlq.validator.service.ValidatorService;
import com.zhlq.validator.service.impl.ValidatorServiceImpl;

/**
 * @ClassName UserValidator
 * @Description 用户验证
 * @author ZHLQ
 * @date 2015年4月15日 下午10:09:24
 */
@Component("userValidator")
public class UserValidator extends ValidatorServiceImpl implements ValidatorService {

	protected static final String USER_CLOSE = "user.close";
	protected static final String USER_OPEN = "user.open";
	protected static final String USER_EDIT = "user.edit";
	protected static final String USER_TOEIDT = "user.toeidt";
	protected static final String USER_LOOK = "user.look";
	protected static final String USER_DELETE = "user.delete";
	protected static final String USER_ADD = "user.add";

	{
		map.put(ADD, USER_ADD);
		map.put(DELETE, USER_DELETE);
		map.put(LOOK, USER_LOOK);
		map.put(TOEIDT, USER_TOEIDT);
		map.put(EDIT, USER_EDIT);
		map.put(OPEN, USER_OPEN);
		map.put(CLOSE, USER_CLOSE);
	}

	@Override
	public List<Validator> validate(String type, Object object) {
		if (this.get(ADD).equals(type)) {
			// 新增
			return validateAdd((User) object);
		} else if (this.get(DELETE).equals(type)) {
			// 删除
			return validateDelete((User) object);
		} else if (this.get(LOOK).equals(type)) {
			// 查看
			return validateLook((User) object);
		} else if (this.get(TOEIDT).equals(type)) {
			// 跳转编辑
			return validateToEdit((User) object);
		} else if (this.get(EDIT).equals(type)) {
			// 编辑
			return validateEdit((User) object);
		} else if (this.get(OPEN).equals(type)) {
			// 启用
			return validateOpen((User) object);
		} else if (this.get(CLOSE).equals(type)) {
			// 禁用
			return validateClose((User) object);
		} else {
			// 其他
			return new ArrayList<Validator>();
		}
	}

	/**
	 * 禁用
	 */
	private List<Validator> validateClose(User user) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(user, validator);
		validateId(user.getId(), validator);
		return validator;
	}

	/**
	 * 启用
	 */
	private List<Validator> validateOpen(User user) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(user, validator);
		validateId(user.getId(), validator);
		return validator;
	}

	/**
	 * 编辑
	 */
	private List<Validator> validateEdit(User user) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(user, validator);
		validateId(user.getId(), validator);
		validateName(user.getName(), validator);
		validatePassword(user.getPassword(), validator);
		validateState(user.getState(), validator);
		return validator;
	}

	/**
	 * 跳转编辑
	 */
	private List<Validator> validateToEdit(User user) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(user, validator);
		validateId(user.getId(), validator);
		return validator;
	}

	/**
	 * 查看
	 */
	private List<Validator> validateLook(User user) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(user, validator);
		validateId(user.getId(), validator);
		return validator;
	}

	/**
	 * 删除
	 */
	private List<Validator> validateDelete(User user) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(user, validator);
		validateId(user.getId(), validator);
		return validator;
	}

	/**
	 * 新增
	 */
	private List<Validator> validateAdd(User user) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(user, validator);
		validateName(user.getName(), validator);
		validatePassword(user.getPassword(), validator);
		validateState(user.getState(), validator);
		return validator;
	}

}
