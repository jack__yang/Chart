package com.zhlq.auth.restype.ctrl.annotation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zhlq.auth.restype.bean.ResourceType;
import com.zhlq.auth.restype.bean.ResourceTypeForm;
import com.zhlq.condition.Query;
import com.zhlq.constant.CtrlConstant;
import com.zhlq.core.service.ProjectService;
import com.zhlq.page.Page;
import com.zhlq.tips.bean.Tips;
import com.zhlq.tips.constant.TipsConstant;
import com.zhlq.util.CollectionUtil;
import com.zhlq.validator.bean.Validator;
import com.zhlq.validator.service.ValidatorService;

/**
 * @ClassName ResourceTypeCtrl
 * @Description 资源类型控制
 * @author ZHLQ
 * @date 2015年4月17日 上午12:14:21
 */
@Controller
public class ResourceTypeCtrl {
	
	private static final String AUTH_RESTYPE_EDIT = "auth/restype/edit";
	private static final String AUTH_RESTYPE_TOEDIT = "auth/restype/toedit";
	private static final String AUTH_RESTYPE_LOOK = "auth/restype/look";
	private static final String AUTH_RESTYPE_DELETE = "auth/restype/delete";
	private static final String AUTH_RESTYPE_ADD = "auth/restype/add";
	private static final String AUTH_RESTYPE_TOADD = "auth/restype/toadd";
	private static final String AUTH_RESTYPE_LIST = "auth/restype/list";
	@Autowired
	private ProjectService projectService;
	@Autowired
	@Qualifier("resourceTypeValidator")
	private ValidatorService validatorService;

	/**
	 * 列表页面
	 */
	@RequestMapping(AUTH_RESTYPE_LIST)
	public String list(HttpServletRequest request, HttpServletResponse response,
			Page page, ResourceTypeForm resourceTypeForm) {
		// 查询数据
		request.setAttribute(CtrlConstant.get(CtrlConstant.PAGE_RESULT), projectService.retrieves(new Query(resourceTypeForm), page));
		request.setAttribute(CtrlConstant.get(CtrlConstant.QUERY), resourceTypeForm);
		return AUTH_RESTYPE_LIST;
	}

	/**
	 * 跳转新增
	 */
	@RequestMapping(AUTH_RESTYPE_TOADD)
	public String toAdd(HttpServletRequest request, HttpServletResponse response) {
		return AUTH_RESTYPE_ADD;
	}

	/**
	 * 新增操作
	 */
	@RequestMapping(AUTH_RESTYPE_ADD)
	public String add(HttpServletRequest request, HttpServletResponse response,
			ResourceType resourceType) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.ADD), resourceType);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 保存数据库
		projectService.create(resourceType);
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), 
				new Tips(Tips.SUCCESS, CtrlConstant.RESOURCE_TYPE_LIST, CtrlConstant.ADD_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

	/**
	 * 删除操作
	 */
	@RequestMapping(AUTH_RESTYPE_DELETE)
	public String delete(HttpServletRequest request, HttpServletResponse response,
			ResourceType resourceType) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.DELETE), resourceType);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 删除数据
		projectService.delete(projectService.retrieveOne(resourceType));
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), 
				new Tips(Tips.SUCCESS, CtrlConstant.RESOURCE_TYPE_LIST, CtrlConstant.DELETE_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

	/**
	 * 查看操作
	 */
	@RequestMapping(AUTH_RESTYPE_LOOK)
	public String look(HttpServletRequest request, HttpServletResponse response,
			ResourceType resourceType) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.LOOK), resourceType);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 查询数据
		request.setAttribute(CtrlConstant.get(CtrlConstant.LOOK), projectService.retrieveOne(resourceType));
		return AUTH_RESTYPE_LOOK;
	}

	/**
	 * 跳转编辑
	 */
	@RequestMapping(AUTH_RESTYPE_TOEDIT)
	public String toEdit(HttpServletRequest request, HttpServletResponse response,
			ResourceType resourceType) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.TOEIDT), resourceType);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 查询数据
		request.setAttribute(CtrlConstant.get(CtrlConstant.UPDATE), projectService.retrieveOne(resourceType));
		return AUTH_RESTYPE_EDIT;
	}

	/**
	 * 编辑操作
	 */
	@RequestMapping(AUTH_RESTYPE_EDIT)
	public String edit(HttpServletRequest request, HttpServletResponse response,
			ResourceType resourceType) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.EDIT), resourceType);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 保存到数据库
		projectService.updateNotNullStr(resourceType);
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), 
				new Tips(Tips.SUCCESS, CtrlConstant.RESOURCE_TYPE_LIST, CtrlConstant.UPDATE_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

}
