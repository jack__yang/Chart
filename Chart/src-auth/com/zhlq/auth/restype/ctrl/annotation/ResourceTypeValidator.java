package com.zhlq.auth.restype.ctrl.annotation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.zhlq.auth.restype.bean.ResourceType;
import com.zhlq.util.StringUtil;
import com.zhlq.validator.bean.Validator;
import com.zhlq.validator.service.ValidatorService;
import com.zhlq.validator.service.impl.ValidatorServiceImpl;

/**
 * @ClassName ResourceTypeValidator
 * @Description 资源树验证
 * @author ZHLQ
 * @date 2015年4月17日 下午8:07:25
 */
@Component("resourceTypeValidator")
public class ResourceTypeValidator extends ValidatorServiceImpl implements ValidatorService {

	protected static final String RESOURCE_CLOSE = "user.close";
	protected static final String RESOURCE_OPEN = "user.open";
	protected static final String RESOURCE_EDIT = "user.edit";
	protected static final String RESOURCE_TOEIDT = "user.toeidt";
	protected static final String RESOURCE_LOOK = "user.look";
	protected static final String RESOURCE_DELETE = "user.delete";
	protected static final String RESOURCE_ADD = "user.add";

	{
		map.put(ADD, RESOURCE_ADD);
		map.put(DELETE, RESOURCE_DELETE);
		map.put(LOOK, RESOURCE_LOOK);
		map.put(TOEIDT, RESOURCE_TOEIDT);
		map.put(EDIT, RESOURCE_EDIT);
		map.put(OPEN, RESOURCE_OPEN);
		map.put(CLOSE, RESOURCE_CLOSE);
	}

	@Override
	public List<Validator> validate(String type, Object object) {
		if (this.get(ADD).equals(type)) {
			// 新增
			return validateAdd((ResourceType) object);
		} else if (this.get(DELETE).equals(type)) {
			// 删除
			return validateDelete((ResourceType) object);
		} else if (this.get(LOOK).equals(type)) {
			// 查看
			return validateLook((ResourceType) object);
		} else if (this.get(TOEIDT).equals(type)) {
			// 跳转编辑
			return validateToEdit((ResourceType) object);
		} else if (this.get(EDIT).equals(type)) {
			// 编辑
			return validateEdit((ResourceType) object);
		} else {
			// 其他
			return new ArrayList<Validator>();
		}
	}

	/**
	 * 编辑
	 */
	private List<Validator> validateEdit(ResourceType resourceType) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(resourceType, validator);
		validateId(resourceType.getId(), validator);
		validateEname(resourceType.getEname(), validator);
		validateCname(resourceType.getCname(), validator);
		validateValue(resourceType.getValue(), validator);
		return validator;
	}

	/**
	 * 跳转编辑
	 */
	private List<Validator> validateToEdit(ResourceType resourceType) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(resourceType, validator);
		validateId(resourceType.getId(), validator);
		return validator;
	}

	/**
	 * 查看
	 */
	private List<Validator> validateLook(ResourceType resourceType) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(resourceType, validator);
		validateId(resourceType.getId(), validator);
		return validator;
	}

	/**
	 * 删除
	 */
	private List<Validator> validateDelete(ResourceType resourceType) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(resourceType, validator);
		validateId(resourceType.getId(), validator);
		return validator;
	}

	/**
	 * 新增
	 */
	private List<Validator> validateAdd(ResourceType resourceType) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(resourceType, validator);
		validateEname(resourceType.getEname(), validator);
		validateCname(resourceType.getCname(), validator);
		validateValue(resourceType.getValue(), validator);
		return validator;
	}

	/**
	 * 编号
	 */
	private void validateValue(String value, List<Validator> validator) {
		if(StringUtil.isEmptyNull(value)){
			validator.add(new Validator("value", "编号", "不能为空"));
		} else if(!this.isOneOrTwoDigit(value)){
			validator.add(new Validator("value", "编号", "必须为数字字符，长度1到2位"));
		}
	}

}
