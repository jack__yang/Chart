package com.zhlq.auth.resource.ctrl.annotation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zhlq.auth.resource.bean.Resource;
import com.zhlq.auth.resource.bean.ResourceForm;
import com.zhlq.auth.restype.bean.ResourceType;
import com.zhlq.condition.Query;
import com.zhlq.condition.Where;
import com.zhlq.constant.CtrlConstant;
import com.zhlq.core.service.ProjectService;
import com.zhlq.page.Page;
import com.zhlq.tips.bean.Tips;
import com.zhlq.tips.constant.TipsConstant;
import com.zhlq.util.CollectionUtil;
import com.zhlq.validator.bean.Validator;
import com.zhlq.validator.service.ValidatorService;

/**
 * @ClassName ResourceCtrl
 * @Description 资源控制
 * @author ZHLQ
 * @date 2015年4月16日 下午10:43:05
 */
@Controller
public class ResourceCtrl {
	
	private static final String AUTH_RESOURCE_EDIT = "auth/resource/edit";
	private static final String AUTH_RESOURCE_TOEDIT = "auth/resource/toedit";
	private static final String AUTH_RESOURCE_LOOK = "auth/resource/look";
	private static final String AUTH_RESOURCE_DELETE = "auth/resource/delete";
	private static final String AUTH_RESOURCE_ADD = "auth/resource/add";
	private static final String AUTH_RESOURCE_TOADD = "auth/resource/toadd";
	private static final String AUTH_RESOURCE_LIST = "auth/resource/list";
	
	@Autowired
	private ProjectService projectService;
	@Autowired
	@Qualifier("resourceValidator")
	private ValidatorService validatorService;

	/**
	 * 列表页面
	 */
	@RequestMapping(AUTH_RESOURCE_LIST)
	public String list(HttpServletRequest request, HttpServletResponse response,
			Page page, ResourceForm resourceForm) {
		// 查询数据
		request.setAttribute(CtrlConstant.get(CtrlConstant.PAGE_RESULT), 
				projectService.retrieves(new Query(resourceForm), page));
		request.setAttribute(CtrlConstant.get(CtrlConstant.QUERY), resourceForm);
		// 资源类型
		Where whereResourceType = new Where(ResourceType.class);
		request.setAttribute("resourceTypes", projectService.retrieve(whereResourceType));
		return AUTH_RESOURCE_LIST;
	}

	/**
	 * 跳转新增
	 */
	@RequestMapping(AUTH_RESOURCE_TOADD)
	public String toAdd(HttpServletRequest request, HttpServletResponse response) {
		// 资源类型
		Where whereResourceType = new Where(ResourceType.class);
		request.setAttribute("resourceTypes", projectService.retrieve(whereResourceType));
		return AUTH_RESOURCE_ADD;
	}

	/**
	 * 新增操作
	 */
	@RequestMapping(AUTH_RESOURCE_ADD)
	public String add(HttpServletRequest request, HttpServletResponse response,
			Resource resource) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.ADD), resource);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 保存数据库
		projectService.create(resource);
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), 
				new Tips(Tips.SUCCESS, CtrlConstant.RESOURCE_LIST, CtrlConstant.ADD_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

	/**
	 * 删除操作
	 */
	@RequestMapping(AUTH_RESOURCE_DELETE)
	public String delete(HttpServletRequest request, HttpServletResponse response,
			Resource resource) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.DELETE), resource);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 删除数据
		projectService.delete(projectService.retrieveOne(resource));
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), 
				new Tips(Tips.SUCCESS, CtrlConstant.RESOURCE_LIST, CtrlConstant.DELETE_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

	/**
	 * 查看操作
	 */
	@RequestMapping(AUTH_RESOURCE_LOOK)
	public String look(HttpServletRequest request, HttpServletResponse response,
			Resource resource) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.LOOK), resource);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 查询数据
		request.setAttribute(CtrlConstant.get(CtrlConstant.LOOK), projectService.retrieveOne(resource));
		// 资源类型
		Where whereResourceType = new Where(ResourceType.class);
		request.setAttribute("resourceTypes", projectService.retrieve(whereResourceType));
		return AUTH_RESOURCE_LOOK;
	}

	/**
	 * 跳转编辑
	 */
	@RequestMapping(AUTH_RESOURCE_TOEDIT)
	public String toEdit(HttpServletRequest request, HttpServletResponse response,
			Resource resource) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.TOEIDT), resource);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 查询数据
		request.setAttribute(CtrlConstant.get(CtrlConstant.UPDATE), projectService.retrieveOne(resource));
		// 资源类型
		Where whereResourceType = new Where(ResourceType.class);
		request.setAttribute("resourceTypes", projectService.retrieve(whereResourceType));
		return AUTH_RESOURCE_EDIT;
	}

	/**
	 * 编辑操作
	 */
	@RequestMapping(AUTH_RESOURCE_EDIT)
	public String edit(HttpServletRequest request, HttpServletResponse response,
			Resource resource) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.EDIT), resource);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 保存到数据库
		projectService.updateNotNullStr(resource);
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), 
				new Tips(Tips.SUCCESS, CtrlConstant.RESOURCE_LIST, CtrlConstant.UPDATE_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

}