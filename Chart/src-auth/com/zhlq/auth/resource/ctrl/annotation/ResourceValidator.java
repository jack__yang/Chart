package com.zhlq.auth.resource.ctrl.annotation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.zhlq.auth.resource.bean.Resource;
import com.zhlq.validator.bean.Validator;
import com.zhlq.validator.service.ValidatorService;
import com.zhlq.validator.service.impl.ValidatorServiceImpl;

/**
 * @ClassName ResourceValidator
 * @Description 资源验证
 * @author ZHLQ
 * @date 2015年4月16日 下午11:32:37
 */
@Component("resourceValidator")
public class ResourceValidator extends ValidatorServiceImpl implements ValidatorService {

	protected static final String RESOURCE_CLOSE = "user.close";
	protected static final String RESOURCE_OPEN = "user.open";
	protected static final String RESOURCE_EDIT = "user.edit";
	protected static final String RESOURCE_TOEIDT = "user.toeidt";
	protected static final String RESOURCE_LOOK = "user.look";
	protected static final String RESOURCE_DELETE = "user.delete";
	protected static final String RESOURCE_ADD = "user.add";

	{
		map.put(ADD, RESOURCE_ADD);
		map.put(DELETE, RESOURCE_DELETE);
		map.put(LOOK, RESOURCE_LOOK);
		map.put(TOEIDT, RESOURCE_TOEIDT);
		map.put(EDIT, RESOURCE_EDIT);
		map.put(OPEN, RESOURCE_OPEN);
		map.put(CLOSE, RESOURCE_CLOSE);
	}

	@Override
	public List<Validator> validate(String type, Object object) {
		if (this.get(ADD).equals(type)) {
			// 新增
			return validateAdd((Resource) object);
		} else if (this.get(DELETE).equals(type)) {
			// 删除
			return validateDelete((Resource) object);
		} else if (this.get(LOOK).equals(type)) {
			// 查看
			return validateLook((Resource) object);
		} else if (this.get(TOEIDT).equals(type)) {
			// 跳转编辑
			return validateToEdit((Resource) object);
		} else if (this.get(EDIT).equals(type)) {
			// 编辑
			return validateEdit((Resource) object);
		} else {
			// 其他
			return new ArrayList<Validator>();
		}
	}

	/**
	 * 编辑
	 */
	private List<Validator> validateEdit(Resource resource) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(resource, validator);
		validateId(resource.getId(), validator);
		validateEname(resource.getEname(), validator);
		validateCname(resource.getCname(), validator);
		validateType(resource.getType(), validator);
		return validator;
	}

	/**
	 * 跳转编辑
	 */
	private List<Validator> validateToEdit(Resource resource) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(resource, validator);
		validateId(resource.getId(), validator);
		return validator;
	}

	/**
	 * 查看
	 */
	private List<Validator> validateLook(Resource resource) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(resource, validator);
		validateId(resource.getId(), validator);
		return validator;
	}

	/**
	 * 删除
	 */
	private List<Validator> validateDelete(Resource resource) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(resource, validator);
		validateId(resource.getId(), validator);
		return validator;
	}

	/**
	 * 新增
	 */
	private List<Validator> validateAdd(Resource resource) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(resource, validator);
		validateEname(resource.getEname(), validator);
		validateCname(resource.getCname(), validator);
		validateType(resource.getType(), validator);
		return validator;
	}

}
