package com.zhlq.auth.bean;



/**
 * ProjFunc entity. @author MyEclipse Persistence Tools
 */

public class ProjFunc  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String no;
     private String projNo;
     private String funcNo;


    // Constructors

    /** default constructor */
    public ProjFunc() {
    }

	/** minimal constructor */
    public ProjFunc(String no) {
        this.no = no;
    }
    
    /** full constructor */
    public ProjFunc(String no, String projNo, String funcNo) {
        this.no = no;
        this.projNo = projNo;
        this.funcNo = funcNo;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return this.no;
    }
    
    public void setNo(String no) {
        this.no = no;
    }

    public String getProjNo() {
        return this.projNo;
    }
    
    public void setProjNo(String projNo) {
        this.projNo = projNo;
    }

    public String getFuncNo() {
        return this.funcNo;
    }
    
    public void setFuncNo(String funcNo) {
        this.funcNo = funcNo;
    }
   








}