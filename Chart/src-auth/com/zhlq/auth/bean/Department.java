package com.zhlq.auth.bean;



/**
 * Department entity. @author MyEclipse Persistence Tools
 */

public class Department  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String no;
     private String ename;
     private String cname;


    // Constructors

    /** default constructor */
    public Department() {
    }

	/** minimal constructor */
    public Department(String no) {
        this.no = no;
    }
    
    /** full constructor */
    public Department(String no, String ename, String cname) {
        this.no = no;
        this.ename = ename;
        this.cname = cname;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return this.no;
    }
    
    public void setNo(String no) {
        this.no = no;
    }

    public String getEname() {
        return this.ename;
    }
    
    public void setEname(String ename) {
        this.ename = ename;
    }

    public String getCname() {
        return this.cname;
    }
    
    public void setCname(String cname) {
        this.cname = cname;
    }
   








}