package com.zhlq.auth.bean;



/**
 * UserProj entity. @author MyEclipse Persistence Tools
 */

public class UserProj  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String no;
     private String userNo;
     private String projNo;


    // Constructors

    /** default constructor */
    public UserProj() {
    }

	/** minimal constructor */
    public UserProj(String no) {
        this.no = no;
    }
    
    /** full constructor */
    public UserProj(String no, String userNo, String projNo) {
        this.no = no;
        this.userNo = userNo;
        this.projNo = projNo;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return this.no;
    }
    
    public void setNo(String no) {
        this.no = no;
    }

    public String getUserNo() {
        return this.userNo;
    }
    
    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getProjNo() {
        return this.projNo;
    }
    
    public void setProjNo(String projNo) {
        this.projNo = projNo;
    }
   








}