package com.zhlq.auth.bean;



/**
 * DeptUser entity. @author MyEclipse Persistence Tools
 */

public class DeptUser  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String no;
     private String deptNo;
     private String userNo;


    // Constructors

    /** default constructor */
    public DeptUser() {
    }

	/** minimal constructor */
    public DeptUser(String no) {
        this.no = no;
    }
    
    /** full constructor */
    public DeptUser(String no, String deptNo, String userNo) {
        this.no = no;
        this.deptNo = deptNo;
        this.userNo = userNo;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return this.no;
    }
    
    public void setNo(String no) {
        this.no = no;
    }

    public String getDeptNo() {
        return this.deptNo;
    }
    
    public void setDeptNo(String deptNo) {
        this.deptNo = deptNo;
    }

    public String getUserNo() {
        return this.userNo;
    }
    
    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }
   








}