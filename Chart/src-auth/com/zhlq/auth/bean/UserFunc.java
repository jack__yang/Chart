package com.zhlq.auth.bean;



/**
 * UserFunc entity. @author MyEclipse Persistence Tools
 */

public class UserFunc  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String no;
     private String userNo;
     private String funcNo;


    // Constructors

    /** default constructor */
    public UserFunc() {
    }

	/** minimal constructor */
    public UserFunc(String no) {
        this.no = no;
    }
    
    /** full constructor */
    public UserFunc(String no, String userNo, String funcNo) {
        this.no = no;
        this.userNo = userNo;
        this.funcNo = funcNo;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return this.no;
    }
    
    public void setNo(String no) {
        this.no = no;
    }

    public String getUserNo() {
        return this.userNo;
    }
    
    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getFuncNo() {
        return this.funcNo;
    }
    
    public void setFuncNo(String funcNo) {
        this.funcNo = funcNo;
    }
   








}