package com.zhlq.auth.bean;



/**
 * ComGroup entity. @author MyEclipse Persistence Tools
 */

public class ComGroup  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String no;
     private String comNo;
     private String groupNo;


    // Constructors

    /** default constructor */
    public ComGroup() {
    }

	/** minimal constructor */
    public ComGroup(String no) {
        this.no = no;
    }
    
    /** full constructor */
    public ComGroup(String no, String comNo, String groupNo) {
        this.no = no;
        this.comNo = comNo;
        this.groupNo = groupNo;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return this.no;
    }
    
    public void setNo(String no) {
        this.no = no;
    }

    public String getComNo() {
        return this.comNo;
    }
    
    public void setComNo(String comNo) {
        this.comNo = comNo;
    }

    public String getGroupNo() {
        return this.groupNo;
    }
    
    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }
   








}