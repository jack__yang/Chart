package com.zhlq.auth.bean;



/**
 * UserRole entity. @author MyEclipse Persistence Tools
 */

public class UserRole  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String no;
     private String userNo;
     private String roleNo;


    // Constructors

    /** default constructor */
    public UserRole() {
    }

	/** minimal constructor */
    public UserRole(String no) {
        this.no = no;
    }
    
    /** full constructor */
    public UserRole(String no, String userNo, String roleNo) {
        this.no = no;
        this.userNo = userNo;
        this.roleNo = roleNo;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return this.no;
    }
    
    public void setNo(String no) {
        this.no = no;
    }

    public String getUserNo() {
        return this.userNo;
    }
    
    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getRoleNo() {
        return this.roleNo;
    }
    
    public void setRoleNo(String roleNo) {
        this.roleNo = roleNo;
    }
   








}