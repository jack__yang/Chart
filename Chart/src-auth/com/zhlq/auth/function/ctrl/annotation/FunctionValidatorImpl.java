package com.zhlq.auth.function.ctrl.annotation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.zhlq.auth.function.bean.Function;
import com.zhlq.validator.bean.Validator;
import com.zhlq.validator.service.impl.ValidatorServiceImpl;

/**
 * @ClassName FunctionValidator
 * @Description 功能验证
 * @author ZHLQ
 * @date 2015年4月16日 下午10:22:07
 */
@Component("functionValidator")
public class FunctionValidatorImpl extends ValidatorServiceImpl implements FunctionValidator {

	{
		map.put(TOALLOT, TOALLOT);
	}
	
	@Override
	public List<Validator> validate(String type, Object object) {
		if (this.get(ADD).equals(type)) {
			// 新增
			return validateAdd((Function) object);
		} else if (this.get(DELETE).equals(type)) {
			// 删除
			return validateDelete((Function) object);
		} else if (this.get(LOOK).equals(type)) {
			// 查看
			return validateLook((Function) object);
		} else if (this.get(TOEIDT).equals(type)) {
			// 跳转编辑
			return validateToEdit((Function) object);
		} else if (this.get(EDIT).equals(type)) {
			// 编辑
			return validateEdit((Function) object);
		} else if (this.get(OPEN).equals(type)) {
			// 启用
			return validateOpen((Function) object);
		} else if (this.get(CLOSE).equals(type)) {
			// 禁用
			return validateClose((Function) object);
		} else if (this.get(TOALLOT).equals(type)) {
			// 跳转分配资源
			return validateToAllot((Function) object);
		} else {
			// 其他
			return new ArrayList<Validator>();
		}
	}

	/**
	 * 跳转分配资源
	 */
	private List<Validator> validateToAllot(Function function) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(function, validator);
		validateId(function.getId(), validator);
		return validator;
	}

	/**
	 * 禁用
	 */
	private List<Validator> validateClose(Function function) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(function, validator);
		validateId(function.getId(), validator);
		return validator;
	}

	/**
	 * 启用
	 */
	private List<Validator> validateOpen(Function function) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(function, validator);
		validateId(function.getId(), validator);
		return validator;
	}

	/**
	 * 编辑
	 */
	private List<Validator> validateEdit(Function function) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(function, validator);
		validateId(function.getId(), validator);
		validateEname(function.getEname(), validator);
		validateCname(function.getCname(), validator);
		return validator;
	}

	/**
	 * 跳转编辑
	 */
	private List<Validator> validateToEdit(Function function) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(function, validator);
		validateId(function.getId(), validator);
		return validator;
	}

	/**
	 * 查看
	 */
	private List<Validator> validateLook(Function function) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(function, validator);
		validateId(function.getId(), validator);
		return validator;
	}

	/**
	 * 删除
	 */
	private List<Validator> validateDelete(Function function) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(function, validator);
		validateId(function.getId(), validator);
		return validator;
	}

	/**
	 * 新增
	 */
	private List<Validator> validateAdd(Function function) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(function, validator);
		validateEname(function.getEname(), validator);
		validateCname(function.getCname(), validator);
		return validator;
	}

}
