package com.zhlq.auth.tree.tool;

import java.util.Comparator;

import com.zhlq.auth.restree.bean.ResourceTreeForm;

public class ResourceTreeFormComparator implements Comparator<ResourceTreeForm> {

	@Override
	public int compare(ResourceTreeForm o1, ResourceTreeForm o2) {
		if(null==o1 && null==o2){
			return 0;
		} else if(null==o1 && null!=o2){
			return -1;
		} else if(null!=o1 && null==o2){
			return 1;
		} else {
			return 0;			
		}
	}

}
