package com.zhlq.auth.tree.dao.hbntmpl;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import com.zhlq.auth.restree.bean.ResourceTreeForm;
import com.zhlq.auth.tree.bean.Node;
import com.zhlq.auth.tree.bean.TemplateTree;
import com.zhlq.auth.tree.bean.TemplateTreeRoleFunc;
import com.zhlq.auth.tree.bean.Tree;
import com.zhlq.auth.tree.bean.TreeForm;
import com.zhlq.auth.tree.constant.TreeConstant;
import com.zhlq.auth.tree.dao.TreeDao;
import com.zhlq.condition.Where;
import com.zhlq.core.dao.hbntmpl.ProjectDaoImpl;
import com.zhlq.exception.ReflectException;
import com.zhlq.util.PropUtil;

public class TreeDaoImpl extends ProjectDaoImpl implements TreeDao {

	@Override
	public TemplateTree getTemplateTree(String name) {
		TemplateTree tree = null;
		// 功能资源树
		Where where = new Where(TreeForm.class);
		where.addEqual("name", name);
		where.orderBy("level ASC, parentId ASC, sort ASC, id ASC");
		List<Object> treeForms = super.retrieve(where);
		List<TreeForm> list = new ArrayList<TreeForm>();
		for(Object o : treeForms) {
			if(null != o) {
				list.add((TreeForm) o);				
			}
		}
		// 获取第一个节点
		if(null!=list && !list.isEmpty()) {
			TreeForm rtf = list.get(0);
			tree = new TemplateTree();
			try {
				PropUtil.copyNotNull(tree, rtf);
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | IntrospectionException e) {
				// 反射异常
				throw new ReflectException("复制对象", e);
			}
			tree.setParent(null);
			tree.setChildren(this.getChildrenNodesByTree(list,tree));
		}
		return tree;
	}

	@Override
	public TemplateTree getTemplateSubtree(String name, Integer parentId) {
		TemplateTree tree = null;
		// 功能资源树
		Where where = new Where(TreeForm.class);
		where.addEqual("name", name);
		where.orderBy("level ASC, parentId ASC, sort ASC, id ASC");
		List<Object> treeForms = super.retrieve(where);
		List<TreeForm> list = new ArrayList<TreeForm>();
		for(Object o : treeForms) {
			if(null != o) {
				list.add((TreeForm) o);				
			}
		}
		// 获取第一个节点
		if(null!=list && !list.isEmpty()) {
			TreeForm rtf = null;
			for(int i = 0; i < list.size(); i++){
				// 寻找父节点
				rtf = list.get(i);
				if(null!=rtf && null!=parentId && parentId.equals(rtf.getId())){
					break;
				} else {
					rtf = null;
				}
			}
			if(null != rtf){
				tree = new TemplateTree();
				try {
					PropUtil.copyNotNull(tree, rtf);
				} catch (IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | IntrospectionException e) {
					// 反射异常
					throw new ReflectException("复制对象", e);
				}
				tree.setParent(null);
				tree.setChildren(this.getChildrenNodesByTree(list,tree));
			}
		}
		return tree;
	}

	@Override
	public TemplateTreeRoleFunc getTemplateSubtreeRoleFunc(String name, Integer parentId, String subtree) {
		TemplateTreeRoleFunc tree = null;
		// 功能资源树
		Where where = new Where(TreeForm.class);
		where.addEqual("name", name);
		where.orderBy("level ASC, parentId ASC, sort ASC, id ASC");
		List<Object> treeForms = super.retrieve(where);
		List<TreeForm> list = new ArrayList<TreeForm>();
		for(Object o : treeForms) {
			if(null != o) {
				list.add((TreeForm) o);				
			}
		}
		// 获取第一个节点
		if(null!=list && !list.isEmpty()) {
			TreeForm rtf = null;
			for(int i = 0; i < list.size(); i++){
				// 寻找父节点
				rtf = list.get(i);
				if(null!=rtf && null!=parentId && parentId.equals(rtf.getId())){
					break;
				} else {
					rtf = null;
				}
			}
			if(null != rtf){
				tree = new TemplateTreeRoleFunc();
				try {
					PropUtil.copyNotNull(tree, rtf);
				} catch (IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | IntrospectionException e) {
					// 反射异常
					throw new ReflectException("复制对象", e);
				}
				tree.setParent(null);
				tree.setSubtree(this.getSubtreeNodesByTreeRoleFunc(list, tree, name, subtree));
				tree.setChildren(this.getChildrenNodesByTree(list,tree));
			}
		}
		return tree;
	}

	@Override
	public TemplateTreeRoleFunc getTemplateTreeRoleFunc(String name, String subtree) {
		TemplateTreeRoleFunc tree = null;
		// 功能资源树
		Where where = new Where(TreeForm.class);
		where.addIn("name", name, subtree);
		where.orderBy("level ASC, parentId ASC, sort ASC, id ASC");
		List<Object> treeForms = super.retrieve(where);
		List<TreeForm> list = new ArrayList<TreeForm>();
		for(Object o : treeForms) {
			if(null != o) {
				list.add((TreeForm) o);				
			}
		}
		// 获取第一个节点
		if(null!=list && !list.isEmpty()) {
			TreeForm rtf = list.get(0);
			tree = new TemplateTreeRoleFunc();
			try {
				PropUtil.copyNotNull(tree, rtf);
				if(name.equals(rtf.getName())){
					PropUtil.copyNotNull(tree.getResource(), rtf.getResource());
					tree.setFunction(null);
				} else if(subtree.equals(rtf.getName())){
					PropUtil.copyNotNull(tree.getFunction(), rtf.getResource());
					tree.setResource(null);
				}
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | IntrospectionException e) {
				// 反射异常
				throw new ReflectException("复制对象", e);
			}
			tree.setParent(null);
			tree.setSubtree(this.getSubtreeNodesByTreeRoleFunc(list, tree, name, subtree));
			tree.setChildren(this.getChildrenNodesByTreeRoleFunc(list, tree, name, subtree));
		}
		return tree;
	}

	@Override
	public List<TreeForm> getTreeForm(String name) {
		List<TreeForm> list = new ArrayList<TreeForm>();
		return this.treeToList(list,this.getTemplateTree(name));
	}

	@Override
	public List<TreeForm> getTreeForm(TemplateTree authTree) {
		List<TreeForm> list = new ArrayList<TreeForm>();
		return this.treeToList(list,authTree);
	}

	@Override
	public List<TreeForm> getTreeFormRoleFunc(TemplateTreeRoleFunc authTree, String name, String subtree) {
		List<TreeForm> list = new ArrayList<TreeForm>();
		return this.treeToListRoleFunc(list, authTree, name, subtree);
	}
	
	private List<Node> getChildrenNodesByTree(List<TreeForm> list, TemplateTree parent){
		List<Node> childrens = new ArrayList<Node>();
		for(int i = 0; i < list.size(); i++){
			if(null != list.get(i)){
				TreeForm rtf = list.get(i);
				if(parent.getId().equals(rtf.getParentId()) && null!=parent.getLevel() && parent.getLevel()+1==rtf.getLevel()){
					TemplateTree children = new TemplateTree();
					try {
						PropUtil.copyNotNull(children, rtf);
					} catch (IllegalAccessException | IllegalArgumentException
							| InvocationTargetException | IntrospectionException e) {
						// 反射异常
						throw new ReflectException("复制对象", e);
					}
					children.setParent(parent);
					children.setChildren(this.getChildrenNodesByTree(list, children));
					childrens.add(children);
				}
			}
		}
		return childrens;
	}
	
	private List<Node> getChildrenNodesByTreeRoleFunc(List<TreeForm> list, TemplateTreeRoleFunc parent, String name, String subtree){
		List<Node> childrens = new ArrayList<Node>();
		for(int i = 0; i < list.size(); i++){
			if(null != list.get(i)){
				TreeForm rtf = list.get(i);
				if(parent.getId().equals(rtf.getParentId()) && null!=parent.getLevel() && parent.getLevel()+1==rtf.getLevel() && name.equals(rtf.getName())){
					TemplateTreeRoleFunc children = new TemplateTreeRoleFunc();
					try {
						PropUtil.copyNotNull(children, rtf);
						if(name.equals(rtf.getName())){
							PropUtil.copyNotNull(children.getResource(), rtf.getResource());
							children.setFunction(null);
						} else if(subtree.equals(rtf.getName())){
							PropUtil.copyNotNull(children.getFunction(), rtf.getResource());
							children.setResource(null);
						}
					} catch (IllegalAccessException | IllegalArgumentException
							| InvocationTargetException | IntrospectionException e) {
						// 反射异常
						throw new ReflectException("复制对象", e);
					}
					children.setParent(parent);
					if("用户目录".equals(children.getResource().getCname())){
						System.out.println(1);
					}
					List<TemplateTreeRoleFunc> s = this.getSubtreeNodesByTreeRoleFunc(list, children, name, subtree);
					children.setSubtree(s);
					children.setChildren(this.getChildrenNodesByTreeRoleFunc(list, children, name, subtree));
					childrens.add(children);
				}
			}
		}
		return childrens;
	}

	private List<TemplateTreeRoleFunc> getSubtreeNodesByTreeRoleFunc(List<TreeForm> list, TemplateTreeRoleFunc parent, String name, String subtree) {
		List<TemplateTreeRoleFunc> childrens = new ArrayList<TemplateTreeRoleFunc>();
		for(int i = 0; i < list.size(); i++){
			if(null != list.get(i)){
				TreeForm rtf = list.get(i);
				if(parent.getId().equals(rtf.getParentId()) && null!=parent.getLevel() && parent.getLevel()+1==rtf.getLevel() && subtree.equals(rtf.getName())){
					TemplateTreeRoleFunc children = new TemplateTreeRoleFunc();
					if("用户目录".equals(parent.getResource().getCname())){
						System.out.println(1);
					}
					try {
						PropUtil.copyNotNull(children, rtf);
						if(name.equals(rtf.getName())){
							PropUtil.copyNotNull(children.getResource(), rtf.getResource());
							children.setFunction(null);
						} else if(subtree.equals(rtf.getName())){
							PropUtil.copyNotNull(children.getFunction(), rtf.getResource());
							children.setResource(null);
						}
					} catch (IllegalAccessException | IllegalArgumentException
							| InvocationTargetException | IntrospectionException e) {
						// 反射异常
						throw new ReflectException("复制对象", e);
					}
					children.setParent(parent);
					if("用户目录".equals(parent.getResource().getCname())){
						System.out.println(1);
					}
					List<TemplateTreeRoleFunc> s = this.getSubtreeNodesByTreeRoleFunc(list, children, name, subtree);
					children.setSubtree(s);
					children.setChildren(this.getChildrenNodesByTreeRoleFunc(list, children, name, subtree));
					childrens.add(children);
				}
			}
		}
		return childrens;
	}

//	@Override
//	public List<TreeForm> getTreeList(Integer id, String type) {
//		if(TreeConstant.FUNCTION.equals(type)){
//			TemplateTree authTree = this.getTree(id, type);
//			return this.treeToList(authTree);
//		} else {
//			return new ArrayList<TreeForm>();			
//		}
//	}

	private List<TreeForm> treeToList(List<TreeForm> list,TemplateTree authTree) {
//		List<TreeForm> list = new ArrayList<TreeForm>();
		if(null == authTree){
			return list;
		}
		TreeForm rtf = new TreeForm(); 
		try {
			PropUtil.copyNotNull(rtf, authTree);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | IntrospectionException e) {
			// 反射异常
			throw new ReflectException("复制对象", e);
		}
		list.add(rtf);
		System.out.println(null!=authTree?authTree.getResource().getCname():"");
		if("用户 目录".equals(authTree.getResource().getCname())){
			System.out.println(1);
		}
		if(null != authTree.getChildren()){
			for(Object o : authTree.getChildren()){
				if(null != o){
					this.treeToList(list, (TemplateTree)o);
				}
			}
		}
		return list;
	}

	private List<TreeForm> treeToListRoleFunc(List<TreeForm> list,TemplateTree authTree, String name, String subtree) {
//		List<TreeForm> list = new ArrayList<TreeForm>();
		if(null == authTree){
			return list;
		}
		TreeForm rtf = new TreeForm(); 
		try {
			PropUtil.copyNotNull(rtf, authTree);
			if(null!=authTree && authTree instanceof TemplateTreeRoleFunc){
				TemplateTreeRoleFunc ttr = (TemplateTreeRoleFunc) authTree;
				if(name.equals(ttr.getName())){
					PropUtil.copyNotNull(rtf.getResource(), ttr.getResource());
					rtf.setFunction(null);
				} else if(subtree.equals(ttr.getName())){
					PropUtil.copyNotNull(rtf.getResource(), ttr.getFunction());
					rtf.setResource(null);
				}
				if(null != ttr.getSubtree()){
					for(Object o : ttr.getSubtree()){
						if(null != o){
							this.treeToListRoleFunc(list, (TemplateTreeRoleFunc)o, name, subtree);
						}
					}
				}
			}

			if(null != authTree.getChildren()){
				for(Object o : authTree.getChildren()){
					if(null != o){
						this.treeToListRoleFunc(list, (TemplateTreeRoleFunc)o, name, subtree);
					}
				}
			}
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | IntrospectionException e) {
			// 反射异常
			throw new ReflectException("复制对象", e);
		}
		list.add(rtf);
//		System.out.println(null!=authTree?authTree.getResource().getCname():"");
//		if("用户 目录".equals(authTree.getResource().getCname())){
//			System.out.println(1);
//		}
//		TemplateTreeRoleFunc ttr = new TemplateTreeRoleFunc();
//		try {
//			PropUtil.copyNotNull(ttr, authTree);
//		} catch (IllegalAccessException | IllegalArgumentException
//				| InvocationTargetException | IntrospectionException e) {
//			// 反射异常
//			throw new ReflectException("复制对象", e);
//		}
		return list;
	}

	@Override
	public TemplateTree getTree(Integer id, String type) {
		TemplateTree authTree = null;
		if(TreeConstant.FUNCTION.equals(type)) {
			// 功能资源树
			Where where = new Where(ResourceTreeForm.class);
			where.addEqual("funId", id);
			where.orderBy("parentId ASC,level ASC,id ASC");
			List<?> resourceTreeForms = super.retrieve(where);
			List<TreeForm> list = new ArrayList<TreeForm>();
			for(Object o : resourceTreeForms) {
				if(null != o) {
					list.add((TreeForm) o);				
				}
			}
			// 获取第一个节点
			if(null!=list && !list.isEmpty()) {
				TreeForm rtf = list.get(0);
				authTree = new TemplateTree();
				try {
					PropUtil.copyNotNull(authTree, rtf);
				} catch (IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | IntrospectionException e) {
					// 反射异常
					throw new ReflectException("复制对象", e);
				}
				authTree.setParent(null);
				authTree.setChildren(this.getChildrenNodes(list,authTree));
			}
		}
		return authTree;
	}
	
	private List<Node> getChildrenNodes(List<TreeForm> list, TemplateTree parent){
		List<Node> childrens = new ArrayList<Node>();
		for(int i = 0; i < list.size(); i++){
			if(null != list.get(i)){
				TreeForm rtf = list.get(i);
				if(parent.getId().equals(rtf.getParentId()) && null!=parent.getLevel() && parent.getLevel()+1==rtf.getLevel()){
					TemplateTree children = new TemplateTree();
					try {
						PropUtil.copyNotNull(children, rtf);
					} catch (IllegalAccessException | IllegalArgumentException
							| InvocationTargetException | IntrospectionException e) {
						// 反射异常
						throw new ReflectException("复制对象", e);
					}
					children.setParent(parent);
					children.setChildren(this.getChildrenNodes(list, children));
					childrens.add(children);
				}
			}
		}
		return childrens;
	}
	
	@Override
	public TemplateTree getTree(Integer roleId) {
		TemplateTree authTree = new TemplateTree();
		Where where = new Where(TreeForm.class);
		where.addEqual("roleId", roleId);
		where.orderBy("parentId ASC,level ASC,id ASC");
		List<?> nodes = super.retrieve(where);
		List<TreeForm> list = new ArrayList<TreeForm>();
		for(Object o : nodes){
			if(null!=o){
				list.add((TreeForm) o);				
			}
		}
		if(null!=nodes && !nodes.isEmpty()){
			ResourceTreeForm rtf = (ResourceTreeForm) nodes.get(0);
			authTree.setId(rtf.getId());
			authTree.setParentId(rtf.getParentId());
			authTree.setLevel(rtf.getLevel());
			authTree.setResId(rtf.getResId());
			authTree.setResource(rtf.getResource());
			authTree.setParent(null);
			authTree.setChildren(this.getChildrenNodes(list,authTree));
		}
		return authTree;
	}

	@Override
	public void deleteTree(TemplateTree authTree) {
		List<Tree> list = this.authTreeToResourceTrees(authTree);
		super.delete(list);
	}

	private List<Tree> authTreeToResourceTrees(TemplateTree authTree) {
		List<Tree> list = new ArrayList<Tree>();
		if(null!=authTree.getChildren() && !authTree.getChildren().isEmpty()){
			for(Object o : authTree.getChildren()){
				if(null != o){
					list.addAll(this.authTreeToResourceTrees((TemplateTree)o));
				}
			}
		}
		list.add(this.authTreeToResourceTree(authTree));
		return list;
	}

	private Tree authTreeToResourceTree(TemplateTree authTree) {
		Tree resourceTree = new Tree();
		resourceTree.setId(authTree.getId());
		resourceTree.setLevel(authTree.getLevel());
		resourceTree.setParentId(authTree.getParentId());
		resourceTree.setResId(resourceTree.getResId());
		return resourceTree;
	}

	@Override
	public TemplateTree getSubTree(Tree tree) {
		// ID、角色ID和深度
		// ID和角色ID
		// 只有ID
		TemplateTree authTree = new TemplateTree();
		Where resourceTreeWhere = new Where(TreeForm.class);
		resourceTreeWhere.addEqual("id", tree.getId());
		List<?> resourceTrees = super.retrieve(resourceTreeWhere);
		if(null!=resourceTrees && resourceTrees.size()==1){
			ResourceTreeForm rtf = (ResourceTreeForm) resourceTrees.get(0);
			authTree.setId(rtf.getId());
			authTree.setParentId(rtf.getParentId());
			authTree.setLevel(rtf.getLevel());
			authTree.setResId(rtf.getResId());
			authTree.setResource(rtf.getResource());
			authTree.setParent(null);
			Where where = new Where(ResourceTreeForm.class);
			where.addEqual("roleId", rtf.getRoleId());
			where.addGreater("level", rtf.getLevel());
			where.orderBy("parentId ASC,level ASC,id ASC");
			List<?> nodes = super.retrieve(where);
			List<TreeForm> list = new ArrayList<TreeForm>();
			for(Object o : nodes){
				if(null != o){
					list.add((TreeForm) o);				
				}
			}
			authTree.setChildren(this.getChildrenNodes(list, authTree));
		}
		return authTree;
	}

}
