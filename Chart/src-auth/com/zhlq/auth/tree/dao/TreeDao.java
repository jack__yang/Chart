package com.zhlq.auth.tree.dao;

import java.util.List;

import com.zhlq.auth.tree.bean.TemplateTree;
import com.zhlq.auth.tree.bean.TemplateTreeRoleFunc;
import com.zhlq.auth.tree.bean.Tree;
import com.zhlq.auth.tree.bean.TreeForm;
import com.zhlq.core.dao.ProjectDao;

public interface TreeDao extends ProjectDao {

	TemplateTree getTemplateTree(String name);

	TemplateTreeRoleFunc getTemplateTreeRoleFunc(String name, String subtree);

	List<TreeForm> getTreeForm(String name);
	
	List<TreeForm> getTreeForm(TemplateTree authTree);

	List<TreeForm> getTreeFormRoleFunc(TemplateTreeRoleFunc authTree, String name, String subtree);

	TemplateTree getTemplateSubtree(String name, Integer parentId);

	TemplateTreeRoleFunc getTemplateSubtreeRoleFunc(String name, Integer parentId, String subtree);
	
	TemplateTree getTree(Integer id, String type);
	
	TemplateTree getTree(Integer roleId);

	void deleteTree(TemplateTree authTree);

	TemplateTree getSubTree(Tree tree);

}
