package com.zhlq.auth.tree.ctrl.annotation;

import com.zhlq.validator.service.ValidatorService;

public interface FunctionTreeValidator extends ValidatorService {

	public static final String FUNCTION_TREE_LIST = "function.tree.list";
	public static final String FUNCTION_TREE_QUERY = "function.tree.query";
	public static final String FUNCTION_TREE_PLUS = "function.tree.plus";
	public static final String FUNCTION_TREE_ADD = "function.tree.add";
	public static final String FUNCTION_TREE_DELETE_SUBTREE_FORCE = "function.delete.subtree.force";
	
}
