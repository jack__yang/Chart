package com.zhlq.auth.tree.ctrl.annotation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.zhlq.auth.tree.bean.Tree;
import com.zhlq.validator.bean.Validator;
import com.zhlq.validator.service.impl.ValidatorServiceImpl;

/**
 * @ClassName TreeValidator
 * @Description 功能资源树验证
 * @author ZHLQ
 * @date 2015年4月16日 下午11:59:27
 */
@Component("roleTreeValidator")
public class RoleTreeValidatorImpl extends ValidatorServiceImpl implements RoleTreeValidator {

	{
		map.put(ROLE_TREE_LIST, ROLE_TREE_LIST);
		map.put(ROLE_TREE_QUERY, ROLE_TREE_QUERY);
		map.put(ROLE_TREE_PLUS, ROLE_TREE_PLUS);
		map.put(ROLE_TREE_ADD, ROLE_TREE_ADD);
		map.put(ROLE_TREE_DELETE_SUBTREE_FORCE, ROLE_TREE_DELETE_SUBTREE_FORCE);
	}

	@Override
	public List<Validator> validate(String type, Object object) {
		if (this.get(ADD).equals(type)) {
			// 新增
		} else if (this.get(DELETE).equals(type)) {
			// 删除
		} else if (this.get(LOOK).equals(type)) {
			// 查看
		} else if (this.get(TOEIDT).equals(type)) {
			// 跳转编辑
		} else if (this.get(EDIT).equals(type)) {
			// 编辑
		} else if (this.get(ROLE_TREE_LIST).equals(type)) {
			// 列表
			validateList((Tree) object);
		} else if (this.get(ROLE_TREE_QUERY).equals(type)) {
			// 查询列表
			validateQuery((Tree) object);
		} else if (this.get(ROLE_TREE_ADD).equals(type)) {
			// 查询列表
			validateAdd((Tree) object);
		} else if (this.get(ROLE_TREE_PLUS).equals(type)) {
			// 查询列表
			validatePlus((Tree) object);
		} else {
			// 其他
			return new ArrayList<Validator>();
		}
		return new ArrayList<Validator>();
	}

	/**
	 * 添加
	 */
	private void validatePlus(Tree tree) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(tree, validator);
		validateResId(tree.getResId(), validator);
	}

	/**
	 * 新增
	 */
	private void validateAdd(Tree tree) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(tree, validator);
	}

	/**
	 * 查询
	 */
	private void validateQuery(Tree tree) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(tree, validator);
		
	}

	/**
	 * 列表
	 */
	private void validateList(Tree tree) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(tree, validator);
	}

	/**
	 * 资源主键
	 */
	private List<Validator> validateResId(Integer resId, List<Validator> validator) {
		if (null == resId) {
			validator.add(new Validator("resId", "资源主键", "不能为空"));
		} else if (resId < 1) {
			validator.add(new Validator("resId", "资源主键", "必须大于0"));
		}
		return validator;
	}

}
