package com.zhlq.auth.tree.bean;

import com.zhlq.auth.function.bean.Function;
import com.zhlq.auth.function.bean.FunctionForm;
import com.zhlq.auth.resource.bean.Resource;
import com.zhlq.auth.resource.bean.ResourceForm;
import com.zhlq.auth.role.bean.Role;
import com.zhlq.auth.role.bean.RoleForm;

public class TreeForm extends Tree {

	/**
	 * @Fields serialVersionUID : 序列化ID
	 */
	private static final long serialVersionUID = 1L;
	public static final String FUNCTION = "FUNCTION";
	public static final String ROLE = "ROLE";
	private String idCompare;
	private String parentIdCompare;
	private String levelCompare;
	private String sortCompare;
	private String resIdCompare;
	private String nameCompare;
	
	private Resource resource;
	private ResourceForm resourceForm;
	private Function function;
	private FunctionForm functionForm;
	private Role role;
	private RoleForm roleForm;
	
	public String getIdCompare() {
		return idCompare;
	}

	public void setIdCompare(String idCompare) {
		this.idCompare = idCompare;
	}

	public String getParentIdCompare() {
		return parentIdCompare;
	}

	public void setParentIdCompare(String parentIdCompare) {
		this.parentIdCompare = parentIdCompare;
	}

	public String getLevelCompare() {
		return levelCompare;
	}

	public void setLevelCompare(String levelCompare) {
		this.levelCompare = levelCompare;
	}

	public String getSortCompare() {
		return sortCompare;
	}

	public void setSortCompare(String sortCompare) {
		this.sortCompare = sortCompare;
	}

	public String getResIdCompare() {
		return resIdCompare;
	}

	public void setResIdCompare(String resIdCompare) {
		this.resIdCompare = resIdCompare;
	}

	public String getNameCompare() {
		return nameCompare;
	}

	public void setNameCompare(String nameCompare) {
		this.nameCompare = nameCompare;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public ResourceForm getResourceForm() {
		return resourceForm;
	}

	public void setResourceForm(ResourceForm resourceForm) {
		this.resourceForm = resourceForm;
	}

	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	public FunctionForm getFunctionForm() {
		return functionForm;
	}

	public void setFunctionForm(FunctionForm functionForm) {
		this.functionForm = functionForm;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public RoleForm getRoleForm() {
		return roleForm;
	}

	public void setRoleForm(RoleForm roleForm) {
		this.roleForm = roleForm;
	}

}
