package com.zhlq.auth.tree.bean;

import com.zhlq.auth.resource.bean.Resource;

public class TemplateTree extends Node {

	private Integer resId;
	private String name;
	private Resource resource;

	public Integer getResId() {
		return resId;
	}

	public void setResId(Integer resId) {
		this.resId = resId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

}
