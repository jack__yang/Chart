package com.zhlq.auth.relation.ctrl.annotation;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zhlq.auth.relation.bean.TreeRelation;
import com.zhlq.auth.relation.bean.TreeRelationForm;
import com.zhlq.auth.role.bean.Role;
import com.zhlq.auth.tree.bean.Node;
import com.zhlq.auth.tree.bean.TemplateTree;
import com.zhlq.auth.tree.bean.TreeForm;
import com.zhlq.auth.tree.service.TreeService;
import com.zhlq.condition.Where;
import com.zhlq.util.CollectionUtil;

@Controller
public class RoleFunctionCtrl {

	private static final String NAME = "ROLE";
	@Autowired
	private TreeService treeService;

	/**
	 * 跳转分配功能
	 */
	@RequestMapping("auth/relation/user/toallot")
	public String toAllot(HttpServletRequest request, HttpServletResponse response,
			TreeRelationForm treeRelationForm) {
		
		// 功能信息
		Where roleWhere = new Where(Role.class);
		roleWhere.addEqual("id", treeRelationForm.getRelationId());
		request.setAttribute("role", treeService.retrieveOneForceException(roleWhere));
		
		// 功能资源树
		TemplateTree authTree = treeService.getTemplateTree(NAME);
		List<TreeForm> authTreeList = treeService.getTreeForm(authTree);
		request.setAttribute("authTreeList", authTreeList);

		// 资源树当前节点
		TemplateTree currentNode = treeService.getCurrentNode(authTree, treeRelationForm.getTreeId());
		request.setAttribute("currentNode", currentNode);
		
		// 资源树当前节点的所有子节点
		List<Node> childNodes = null==currentNode?new ArrayList<Node>():currentNode.getChildren();
		if(!CollectionUtil.isEmptyOrNull(childNodes)){
			request.setAttribute("childNodes", childNodes);				
		}
		
		// 已经勾选的节点
		Where treeRelationWhere = new Where(TreeRelation.class);
		treeRelationWhere.add("relationId", treeRelationForm.getRelationId());
		treeRelationWhere.add("name", "ROLE");
		List<Object> list = treeService.retrieve(treeRelationWhere);
		request.setAttribute("treeRelations", list);
		
//		// 资源类型
//		Where whereResourceType = new Where(ResourceType.class);
//		request.setAttribute("resourceTypes", treeService.retrieve(whereResourceType));

		return "auth/relation/user/allot";
	}

	/**
	 * 跳转分配功能
	 */
	@RequestMapping("auth/relation/user/allot")
	public String allot(HttpServletRequest request, HttpServletResponse response,
			TreeRelationForm treeRelationForm, RedirectAttributes attr) {
		TreeRelation treeRelation = new TreeRelation();
		treeRelation.setTreeId(treeRelationForm.getTreeId());
		treeRelation.setRelationId(treeRelationForm.getRelationId());
		treeRelation.setName("ROLE");
		treeService.create(treeRelation);

		// 当前节点
		treeRelationForm.setTreeId(treeRelationForm.getCurrentTreeId());
		attr.addFlashAttribute(treeRelationForm);
		return "redirect:/auth/relation/user/toallot.do";
	}
	
}
