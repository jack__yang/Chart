package com.zhlq.auth.relation.bean;

/**
 * TreeRelation entity. @author MyEclipse Persistence Tools
 */

public class TreeRelation implements java.io.Serializable {

	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private Integer id;
	private Integer treeId;
	private Integer relationId;
	private String name;
	
	private String tmp = "0";

	// Constructors

	/** default constructor */
	public TreeRelation() {
	}

	/** full constructor */
	public TreeRelation(Integer treeId, Integer relationId, String name) {
		this.treeId = treeId;
		this.relationId = relationId;
		this.name = name;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTreeId() {
		return this.treeId;
	}

	public void setTreeId(Integer treeId) {
		this.treeId = treeId;
	}

	public Integer getRelationId() {
		return this.relationId;
	}

	public void setRelationId(Integer relationId) {
		this.relationId = relationId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTmp() {
		return tmp;
	}

	public void setTmp(String tmp) {
		this.tmp = "0";
	}

}