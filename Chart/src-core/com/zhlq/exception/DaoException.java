package com.zhlq.exception;

/**
 * @ClassName DaoException
 * @Description dao异常
 * @author ZHLQ
 * @date 2014年9月11日 下午9:56:13
 */
public class DaoException extends BaseException {

	/**
	 * @Fields serialVersionUID : 序列化编号
	 */
	private static final long serialVersionUID = 1L;

	public DaoException() {
		super();
	}

	public DaoException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}

	public DaoException(String message) {
		super(message);
	}

	public DaoException(Throwable cause) {
		super(cause);
	}

}
