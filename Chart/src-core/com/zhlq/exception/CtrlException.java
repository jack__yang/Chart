package com.zhlq.exception;

public class CtrlException extends BaseException {

	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 1L;

	public CtrlException() {
		super();
	}

	public CtrlException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CtrlException(String message, Throwable cause) {
		super(message, cause);
	}

	public CtrlException(String message) {
		super(message);
	}

	public CtrlException(Throwable cause) {
		super(cause);
	}

}
