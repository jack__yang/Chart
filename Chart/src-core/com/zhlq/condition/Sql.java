package com.zhlq.condition;

public interface Sql {

	// constant
	public static final String TRUE = "1=1";
	public static final String FALSE = "1=0";
	public static final String SPACE = " ";

	// insert
	public static final String INSERT_INTO = "INSERT INTO";
	public static final String VALUES = "VALUES";

	// select
	public static final String SELECT = "SELECT";
	public static final String INTO = "INTO";
	public static final String FROM = "FROM";

	// update
	public static final String UPDATE = "UPDATE";
	public static final String SET = "SET";

	// delete
	public static final String DELETE_FROM = "DELETE FROM";

	// assist
	public static final String DISTINCT = "DISTINCT";
	public static final String AS = "AS";

	// join
	public static final String JOIN = "JOIN";
	public static final String INNER_JOIN = "INNER JOIN";
	public static final String LEFT_JOIN = "LEFT JOIN";
	public static final String LEFT_OUTER_JOIN = "LEFT OUTER JOIN";
	public static final String RIGHT_JOIN = "RIGHT JOIN";
	public static final String RIGHT_OUTER_JOIN = "RIGHT OUTER JOIN";
	public static final String FULL_JOIN = "FULL JOIN";
	public static final String FULL_OUTER_JOIN = "FULL OUTER JOIN";
	public static final String ON = "ON";

	// union
	public static final String UNION = "UNION";
	public static final String UNION_ALL = "UNION ALL";

	// where
	public static final String WHERE = "WHERE";
	public static final String WHERE_TRUE = "WHERE" + SPACE + TRUE;
	public static final String WHERE_FALSE = "WHERE" + SPACE + FALSE;
	public static final String AND = "AND";
	public static final String OR = "OR";
	public static final String NOT = "NOT";
	public static final String LIKE = "LIKE";
	public static final String IN = "IN";
	public static final String BETWEEN = "BETWEEN";

	// order
	public static final String ORDER_BY = "ORDER BY";
	public static final String DESC = "DESC";
	public static final String ASC = "ASC";

	// symbol
	public static final String EQUAL = "=";
	public static final String ALL = "*";
	public static final String PERCENT = "%";
	public static final String UNDERLINE = "_";
	public static final String XOR = "^";
	public static final String EXCLAMATION = "!";
	public static final String POUND = "#";
	public static final String SINGLE_QUOTE = "'";
	public static final String DOUBLE_QUOTE = "\"";
	public static final String PARENTHESES = "()";
	public static final String PARENTHESES_LEFT = "(";
	public static final String PARENTHESES_RIGHT = ")";
	public static final String BRACKET = "[]";
	public static final String BRACKET_LEFT = "[";
	public static final String BRACKET_RIGHT = "]";
	public static final String SEMICOLON = ";";
	public static final String QUESTION = "?";

	// create
	public static final String CREATE_DATABASE = "CREATE DATABASE";
	public static final String CREATE_TABLE = "CREATE TABLE";

	// CONSTRAINT

	// format
	public static final String TAB = "\t";
	public static final String TAB_SPACE = "    ";
	public static final String NEWLINE = "\n";

	// custom
}
