package com.zhlq.condition;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.zhlq.util.ClassUtil;

/**
 * @ClassName Query
 * @Description 查询对象
 * @author ZHLQ
 * @date 2015年5月26日 上午3:56:11
 */
@SuppressWarnings("rawtypes")
public class Query {

	private List<Condition> list = new ArrayList<Condition>();
	private Class clazz;
	private static String SUFFIX = "Compare";

	/**
	 * 创建一个新的实例 Query。
	 * <p>Title: Query</p>
	 * <p>Description: 需要传入查询实体的class对象</p>
	 */
	public Query(Class<?> clazz) {
		super();
		this.clazz = clazz;
	}

	/**
	 * 创建一个新的实例 Query。
	 * <p>Title: Query</p>
	 * <p>Description: 需要传入查询实体</p>
	 */
	public Query(Object object) {
		super();
		this.clazz = object.getClass().getSuperclass();
		// 遍历每一个属性
		Field[] fields = object.getClass().getSuperclass().getDeclaredFields();
		Field[] compareFields = object.getClass().getDeclaredFields();
		for(Field field : fields){
			String fieldName = field.getName();
			String fieldCompareName = fieldName + SUFFIX;
			// 找到对应的比较符属性
			Object fieldCompareValue = null;
			for(Field fieldCompare : compareFields){
				if(fieldCompareName.equals(fieldCompare.getName())){
					try{
						Method method = object.getClass().getDeclaredMethod("get"+fieldCompareName.substring(0, 1).toUpperCase()+fieldCompareName.substring(1));
						if(null != method){
							fieldCompareValue = method.invoke(object);
							break;
						}
					}catch(NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e){
						// 反射异常：调用方法
						this.doing();
					}
				}
			}
			Object fieldValue = null;
			try{
				Method method = object.getClass().getSuperclass().getDeclaredMethod("get"+fieldName.substring(0, 1).toUpperCase()+fieldName.substring(1));
				if(null!=method){
					fieldValue = method.invoke(object);
				}
			}catch(NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e){
				// 反射异常：调用方法
				this.doing();
			}
			Condition condition;
			if(ClassUtil.isArrayBaseOrString(fieldValue) || ClassUtil.isCollectionBaseOrString(fieldValue)){
				condition = new Condition(fieldName, "in", fieldValue);
			} else {
				String compare = null==fieldCompareValue?null:fieldCompareValue.toString();
				condition = new Condition(fieldName, compare, fieldValue);
			}
			list.add(condition);
		}
	}

	/**
	 * add
	 *
	 * @Title add
	 * @Description 添加查询条件
	 * @param object 查询实体
	 * @return Query 返回类型
	 * @throws
	 */
	public Query add(Object object) {
		Query q = new Query(object);
		this.list.addAll(q.getList());
		return this;
	}

	/**
	 * add
	 *
	 * @Title add
	 * @Description 添加查询条件
	 * @param condition 查询条件对象
	 * @return Query 返回类型
	 * @throws
	 */
	public Query add(Condition condition) {
		list.add(condition);
		return this;
	}

	/**
	 * add
	 *
	 * @Title add
	 * @Description 添加查询条件
	 * @param key 键
	 * @param value 值
	 * @return Query 返回类型
	 * @throws
	 */
	public Query add(String key, String value) {
		list.add(new Condition(key, value));
		return this;
	}

	/**
	 * add
	 *
	 * @Title add
	 * @Description 添加查询条件
	 * @param key 键
	 * @param compare 比较符
	 * @param value 值
	 * @return Query 返回类型
	 * @throws
	 */
	public Query add(String key, String compare, String value) {
		list.add(new Condition(key, compare, value));
		return this;
	}

	/**
	 * toWhere
	 *
	 * @Title toWhere
	 * @Description 转换为Where对象
	 * @return Where 返回类型
	 * @throws
	 */
	public Where toWhere() {
		Where where = new Where(clazz);
		for(Condition c : list){
			where.add(c);
		}
		return where;
	}

	private List<Condition> getList() {
		return list;
	}

	/**
	 * 异常处理
	 */
	private void doing() {
		// 不需要任何处理
	}

}