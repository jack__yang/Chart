package com.zhlq.export.service.excel;

import java.io.File;
import java.io.OutputStream;

import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import com.zhlq.export.bean.Data;
import com.zhlq.export.bean.Header;
import com.zhlq.export.service.ExportService;

public interface ExcelExportService extends ExportService {

	void writeHeader(Header header);

	void writeHeader(Header header, WritableSheet writableSheet);

	void writeHeader(Header header, WritableWorkbook writableWorkbook);
	
	void writeData(Data data);
	
	void close();
	
	void close(WritableWorkbook writableWorkbook);

	void setPath(String path);

	void setFile(File file);

	void setOutputStream(OutputStream outputStream);
	
}
