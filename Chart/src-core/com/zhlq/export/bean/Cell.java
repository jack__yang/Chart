package com.zhlq.export.bean;

import jxl.write.Label;

public class Cell extends Label {

	public Cell(int c, int r, String cont) {
		super(c, r, cont);
	}
	
	public Cell copyByCol(int c) {
		return new Cell(c, this.getRow(), this.getString());
	}
	
	public Cell copyByRow(int r) {
		return new Cell(this.getColumn(), r, this.getString());
	}

}
