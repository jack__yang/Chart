package com.zhlq.export.bean;

import java.util.ArrayList;
import java.util.List;

public class Data {
	
	private List<Row> rows = new ArrayList<Row>();

	public List<Row> getRows() {
		return rows;
	}

	public void setRows(List<Row> rows) {
		this.rows = rows;
	}

	public Data add(Row row) {
		this.rows.add(row);
		return this;
	}

	public Data add(int r, String... values) {
		String value;
		Cell cell;
		Row row = new Row();
		for(int i = 0; i < values.length; i++){
			value = values[i];
			cell = new Cell(i, r, null==value?"":value);
			row.add(cell);
		}
		this.rows.add(row);
		return this;
	}

}
