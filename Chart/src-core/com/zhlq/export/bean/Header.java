package com.zhlq.export.bean;

import java.util.List;

public class Header extends Row {

	public Header() {
		super();
	}

	public Header(List<String> list) {
		super();
		for(int i = 0; i < list.size(); i++){
			super.cells.add(new Cell(i, 0, list.get(i)));
		}
	}
	
	public Header add(String value){
		super.cells.add(new Cell(super.cells.size(), 0, value));
		return this;
	}

}
