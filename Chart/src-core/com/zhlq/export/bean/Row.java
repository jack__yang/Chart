package com.zhlq.export.bean;

import java.util.ArrayList;
import java.util.List;

public class Row {
	
	protected List<Cell> cells = new ArrayList<Cell>();

	public List<Cell> getCells() {
		return cells;
	}

	public void setCells(List<Cell> cells) {
		this.cells = cells;
	}
	
	public Row add(Cell cell){
		cells.add(cell);
		return this;
	}

}
