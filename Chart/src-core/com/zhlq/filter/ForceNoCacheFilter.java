package com.zhlq.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 * 使Browser不缓存页面
 */
public class ForceNoCacheFilter implements Filter {

	/**
	 * 销毁处理
	 */
	public void destroy() {
		// 不需要处理
	}

	/**
	 * 转发前处理
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletResponse responseHttp = (HttpServletResponse) response;
		responseHttp.setHeader("Cache-Control", "no-cache");
		responseHttp.setHeader("Pragma", "no-cache");
		responseHttp.setDateHeader("Expires", -1);
		chain.doFilter(request, response);
	}

	/**
	 * 初始化处理
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// 不需要处理
	}

}
