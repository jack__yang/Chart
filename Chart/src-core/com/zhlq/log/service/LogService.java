package com.zhlq.log.service;

import javax.servlet.http.HttpServletRequest;

import com.zhlq.core.service.ProjectService;
import com.zhlq.log.bean.Visit;

public interface LogService extends ProjectService {

	Visit getVisit(HttpServletRequest request);

}
