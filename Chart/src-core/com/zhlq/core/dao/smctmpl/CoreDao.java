package com.zhlq.core.dao.smctmpl;

import java.util.List;

import com.zhlq.condition.Where;
import com.zhlq.page.Page;

/**
 * @ClassName CoreDao
 * @Description 核心Dao接口
 * @author ZHLQ
 * @param <E>
 * @date 2015年3月15日 下午3:31:37
 */
public interface CoreDao {

	void create(Object object);

	List<Object>  retrieve(Object object);

	List<Object> retrieve(Where where);

	List<Object> retrieve(Where where, Page page);

	void update(Object object);
	
	void delete(Object object);

	void delete(java.util.Collection<?> collection);

	long count();

	long count(Where where);

}