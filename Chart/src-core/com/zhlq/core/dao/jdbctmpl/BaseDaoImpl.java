package com.zhlq.core.dao.jdbctmpl;

import java.util.List;

import com.zhlq.condition.Query;
import com.zhlq.condition.Where;
import com.zhlq.core.dao.BaseDao;
import com.zhlq.page.Page;
import com.zhlq.page.PageResult;

/**
 * @ClassName BaseDaoImpl
 * @Description 基础DaoImpl
 * @author ZHLQ
 * @date 2014年9月11日 下午9:01:57
 */
public class BaseDaoImpl extends CoreDaoImpl implements BaseDao {

	@Override
	public void create(Object object) {
		if(null == object){
			// 不需要处理
		} else {
			super.create(object);			
		}
	}

	@Override
	public void createOrUpdate(Object object) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Object> retrieve(Object object) {
		return super.retrieve(object);
	}

	@Override
	public List<Object> retrieve(Where where) {
		return this.retrieve(where, null);
	}

	@Override
	public List<Object> retrieve(Where where, Page page) {
		return super.retrieve(where, page);
	}

	@Override
	public PageResult retrieves(Where where, Page page) {
		PageResult pageResult = new PageResult();
		pageResult.setDatas(this.retrieve(where, page));
		page.setCount(this.count(where));
		pageResult.setPage(page);
		return pageResult;
	}

	@Override
	public PageResult retrieves(Query query, Page page) {
		PageResult pageResult = new PageResult();
		Where where = query.toWhere();
		pageResult.setDatas(this.retrieve(where, page));
		page.setCount(this.count(where));
		pageResult.setPage(page);
		return pageResult;
	}

	@Override
	public void update(Object object) {
		super.update(object);
	}

	@Override
	public void delete(Object object) {
		if(object instanceof java.util.Collection<?>){
			super.delete((java.util.Collection<?>)object);
		} else {
			super.delete(object);
		}
	}

	@Override
	public long count(Object object) {
		return super.count(Where.beanToWhere(object));
	}

	@Override
	public long count(Where where) {
		return super.count(where);
	}

	@Override
	public List<Object> retrieve(Query query) {
		// TODO Auto-generated method stub
		return null;
	}

}