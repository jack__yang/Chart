package com.zhlq.core.service.impl;

import java.util.List;

import com.zhlq.condition.Query;
import com.zhlq.condition.Where;
import com.zhlq.core.dao.BaseDao;
import com.zhlq.core.service.BaseService;
import com.zhlq.page.Page;
import com.zhlq.page.PageResult;

/**
 * @ClassName BaseServiceImpl
 * @Description 基础ServiceImpl
 * @author ZHLQ
 * @date 2014年9月10日 下午11:35:20
 */
public class BaseServiceImpl implements BaseService {
	
	/**
	 * @Fields baseDao : 基础Dao
	 */
	private BaseDao baseDao;

	@Override
	public void create(Object object) {
		baseDao.create(object);
	}

	@Override
	public void createOrUpdate(Object object) {
		baseDao.createOrUpdate(object);
	}

	@Override
	public List<Object> retrieve(Object object) {
		return baseDao.retrieve(object);
	}

	@Override
	public List<Object> retrieve(Where where) {
		return baseDao.retrieve(where);
	}

	@Override
	public List<Object> retrieve(Where where, Page page) {
		return baseDao.retrieve(where, page);
	}

	@Override
	public List<Object> retrieve(Query query) {
		return baseDao.retrieve(query);
	}

	@Override
	public PageResult retrieves(Where where, Page page) {
		return baseDao.retrieves(where, page);
	}

	@Override
	public PageResult retrieves(Query query, Page page) {
		return baseDao.retrieves(query, page);
	}

	@Override
	public void update(Object object) {
		baseDao.update(object);
	}

	@Override
	public void delete(Object object) {
		baseDao.delete(object);
	}

	@Override
	public long count(Where where) {
		return baseDao.count(where);
	}

	public BaseDao getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(BaseDao baseDao) {
		this.baseDao = baseDao;
	}

}
