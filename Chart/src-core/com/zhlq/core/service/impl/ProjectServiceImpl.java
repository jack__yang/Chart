package com.zhlq.core.service.impl;

import com.zhlq.condition.Where;
import com.zhlq.core.dao.ProjectDao;
import com.zhlq.core.service.ProjectService;
import com.zhlq.exception.DataException;

/**
 * @ClassName ProjectServiceImpl
 * @Description 项目ServiceImpl
 * @author ZHLQ
 * @date 2014年9月6日 下午5:38:02
 */
public class ProjectServiceImpl extends BaseServiceImpl implements
		ProjectService {
	
	private ProjectDao projectDao;

	@Override
	public Object retrieveOne(Object object) {
		return projectDao.retrieveOne(object);
	}

	@Override
	public Object retrieveOne(Where where) {
		return projectDao.retrieveOne(where);
	}

	@Override
	public Object retrieveOneException(Object object) throws DataException {
		return projectDao.retrieveOneException(object);
	}

	@Override
	public Object retrieveOneException(Where where) throws DataException {
		return projectDao.retrieveOneException(where);
	}

	@Override
	public Object retrieveOneForce(Object object) {
		return projectDao.retrieveOneForce(object);
	}

	@Override
	public Object retrieveOneForce(Where where) {
		return projectDao.retrieveOneForce(where);
	}

	@Override
	public Object retrieveOneForceException(Object object) throws DataException {
		return projectDao.retrieveOneForceException(object);
	}

	@Override
	public Object retrieveOneForceException(Where where) throws DataException {
		return projectDao.retrieveOneForceException(where);
	}

	@Override
	public void updateNotNullStr(Object object) {
		projectDao.updateNotNullStr(object);
	}

	public ProjectDao getProjectDao() {
		return projectDao;
	}

	public void setProjectDao(ProjectDao projectDao) {
		this.projectDao = projectDao;
	}

}
