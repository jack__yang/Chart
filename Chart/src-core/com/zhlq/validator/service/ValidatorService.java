package com.zhlq.validator.service;

import java.util.List;

import com.zhlq.validator.bean.Validator;

public interface ValidatorService {

	public static final String CLOSE = "close";
	public static final String OPEN = "open";
	public static final String EDIT = "edit";
	public static final String TOEIDT = "toeidt";
	public static final String LOOK = "look";
	public static final String DELETE = "delete";
	public static final String ADD = "add";
	
	public static final String ONE_OR_TWO_DIGIT = "^[0-9]{1,2}$";
	public static final String EXPORT = "export";
	public static final String DOWNLOAD = "download";
	public static final String IMPORT = "import";
	
	public String get(String key);

	public List<Validator> validate(String type, Object object);

	public boolean isPattern(String value, String pattern);
	
	public boolean isOneOrTwoDigit(String value);

}
