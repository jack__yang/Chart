package com.zhlq.validator.bean;

import java.io.Serializable;

public class Validator implements Serializable {

	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 1L;
	private String fieldName;
	private String chineseName;
	private String tips;

	public Validator() {
		super();
	}

	public Validator(String fieldName, String chineseName, String tips) {
		super();
		this.fieldName = fieldName;
		this.chineseName = chineseName;
		this.tips = tips;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getChineseName() {
		return chineseName;
	}

	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}

	public String getTips() {
		return tips;
	}

	public void setTips(String tips) {
		this.tips = tips;
	}

}
