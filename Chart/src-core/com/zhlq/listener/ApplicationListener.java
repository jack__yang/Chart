package com.zhlq.listener;

import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public final class ApplicationListener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent sce) {
		ServletContext ctx = sce.getServletContext();
		Enumeration<?> names = ctx.getAttributeNames();
		while(names.hasMoreElements()){
			ctx.removeAttribute(String.valueOf(names.nextElement()));
		}
	}

	public void contextInitialized(ServletContextEvent sce) {
		ServletContext ctx = sce.getServletContext();
		ctx.setAttribute("ctx", "/Auth/");
		ctx.setAttribute("website", "http://localhost:8080");
		ctx.setAttribute("aTitle", "Auth-ZHLQ");
	}

}
