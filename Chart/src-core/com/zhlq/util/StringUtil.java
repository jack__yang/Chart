package com.zhlq.util;

public final class StringUtil {

	private StringUtil() {
		super();
	}

	public static boolean isEmpty(String value) {
		if (null != value) {
			return "".equals(value.trim());
		} else {
			return false;
		}
	}

	public static boolean isEmpty(Object value) {
		if (null != value && String.class.equals(value.getClass())) {
			return "".equals(((String) value).trim());
		} else {
			return false;
		}
	}

	public static boolean isNotEmpty(String value) {
		if (null != value) {
			return !"".equals(value.trim());
		} else {
			return false;
		}
	}

	public static boolean isNotEmpty(Object value) {
		if (null != value && String.class.equals(value.getClass())) {
			return !"".equals(((String) value).trim());
		} else {
			return false;
		}
	}

	public static boolean isEmptyNull(String value) {
		if (null != value) {
			return "".equals(value.trim());
		} else {
			return true;
		}
	}

	public static boolean isEmptyNull(Object value) {
		if (null != value && String.class.equals(value.getClass())) {
			return "".equals(((String) value).trim());
		} else {
			return true;
		}
	}

	public static boolean isNotEmptyNull(String value) {
		if (null != value) {
			return !"".equals(value.trim());
		} else {
			return true;
		}
	}

	public static boolean isNotEmptyNull(Object value) {
		if (null != value && String.class.equals(value.getClass())) {
			return !"".equals(((String) value).trim());
		} else {
			return true;
		}
	}

}
