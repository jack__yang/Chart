package com.zhlq.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @ClassName SerializeUtil
 * @Description 序列化工具
 * @author ZHLQ
 * @date 2015年6月7日 上午1:55:41
 */
public class SerializeUtil {

	/**
	 * write
	 *
	 * @Title write
	 * @Description 写对象
	 * @param object对象
	 * @param path路径
	 * @throws FileNotFoundException
	 * @throws IOException 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	public static void write(Object object, String path)
			throws FileNotFoundException, IOException {
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(new FileOutputStream(path));
			oos.writeObject(object);
		} finally {
			if(null != oos){
				oos.close();
				oos = null;
			}
		}
	}

	/**
	 * read
	 *
	 * @Title read
	 * @Description 读对象
	 * @param path路径
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException 设定文件
	 * @return Object 返回类型
	 * @throws
	 */
	public static Object read(String path) throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream ois = null;
		Object object;
		try {
			ois = new ObjectInputStream(new FileInputStream(path));
			object = ois.readObject();
		} finally {
			if(null != ois){
				ois.close();	
				ois = null;
			}
		}
		return object;
	}

	/**
	 * 创建一个新的实例 SerializeUtil。 Title: SerializeUtil Description: 私有无参构造方法
	 */
	private SerializeUtil() {
		super();
	}
}
