package com.zhlq.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class SpringContextUtil {
	
	private static String[] path = {
		"/WEB-INF/config/spring/applicationContext-dataSource.xml",
		"/WEB-INF/config/spring/applicationContext-sessionFactory.xml",
		"/WEB-INF/config/spring/applicationContext-core.xml",
		"/WEB-INF/config/spring/applicationContext-transaction.xml",
		"/WEB-INF/config/spring/applicationContext-tree.xml"};
	private static ApplicationContext context;

	public static ApplicationContext getApplicationContext(String path){
		ApplicationContext context = new ClassPathXmlApplicationContext(path);
		//"applicationContext*.xml"
		return context;
	}
	
	public static ApplicationContext getApplicationContext(String[] path){
		////{"application.xml",applicationContext*.xml"}
		ApplicationContext context = new ClassPathXmlApplicationContext(path);
		return context;
	}

	public static ApplicationContext getApplicationContextByFile(String path){
		ApplicationContext context = new FileSystemXmlApplicationContext(path);
		//"WebRoot/WEB-INF/applicationContext*.xml"
		return context;
	}
	
	public static ApplicationContext getApplicationContextByFile(String[] path){
		//{"WebRoot/WEB-INF/application1.xml",WebRoot/WEB-INF/applicationContext*.xml"}
		ApplicationContext context = new FileSystemXmlApplicationContext(path);
		return context;
	}
	
	public static Object getBean(String id){
		if(null == SpringContextUtil.context){
			SpringContextUtil.context = SpringContextUtil.getApplicationContextByFile(path);
		}
		return context.getBean(id);
	}
	
	public static Object getBean(String id, String[] path){
		if(null == SpringContextUtil.context){
			SpringContextUtil.context = SpringContextUtil.getApplicationContextByFile(path);
		}
		return context.getBean(id);
	}
	
}
