package com.zhlq.util;


/**
 * @ClassName CurrentUtil
 * @Description 当前值工具
 * @author ZHLQ
 * @date 2015年4月16日 上午12:07:51
 */
public final class CurrentUtil {

	/**
	 * 当前方法名
	 */
	public static String currentMethod(Throwable throwable) {
		return throwable.getStackTrace()[0].getMethodName();
	}
	
	/**
	 * 当前方法名（权全限定名）
	 */
	public static String currentMethodQualified(Throwable throwable) {
		StackTraceElement stackTraceElement = throwable.getStackTrace()[0];
		return stackTraceElement.getClassName()+"."+stackTraceElement.getMethodName();
	}

	/**
	 * 当前类名
	 */
	public static String currentClass(Throwable throwable) {
		String className = throwable.getStackTrace()[0].getClassName();
		return className.substring(className.lastIndexOf('.')+1);
	}

	/**
	 * 当前类名（全限定名）
	 */
	public static String currentClassQualified(Throwable throwable) {
		return throwable.getStackTrace()[0].getClassName();
	}

	/**
	 * 创建一个新的实例 CurrentUtil。
	 * <p>Title: CurrentUtil</p>
	 * <p>Description: 私有无参构造方法</p>
	 */
	private CurrentUtil() {
		super();
	}

	/**
	 * 测试使用
	 */
	public static void main(String[] args) {
		System.out.println(CurrentUtil.currentClass(new Throwable()));
	}

}
