package com.zhlq.util;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public final class PropUtil {

	private PropUtil() {
		super();
	}

	public static Object copy(Object dest, Object res) throws IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		//目标对象
		BeanInfo destBeanInfo = Introspector.getBeanInfo(dest.getClass());
		PropertyDescriptor[] destPropertyDescriptors = destBeanInfo.getPropertyDescriptors();
		//原始对象
		BeanInfo resBeanInfo = Introspector.getBeanInfo(res.getClass());
		PropertyDescriptor[] resPropertyDescriptors = resBeanInfo.getPropertyDescriptors();
			
		for(PropertyDescriptor resPd : resPropertyDescriptors){
			String resName = resPd.getName();
			Method getMethod = resPd.getReadMethod();
			Object value = getMethod.invoke(res);
			// 不复制class属性
			if(!"class".equals(resName)){
				for(PropertyDescriptor destPd : destPropertyDescriptors){
					if(destPd.getName().equals(resName) && (destPd.getPropertyType()==resPd.getPropertyType())){
						Method setMethod = destPd.getWriteMethod();
						setMethod.invoke(dest, value);
						break;
					}
				}
			}
		}
		return dest;
	}

	public static Object copyNotNull(Object dest, Object res) throws IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		if(null==res || null==dest){
			return null;
		}
		//目标对象
		BeanInfo destBeanInfo = Introspector.getBeanInfo(dest.getClass());
		PropertyDescriptor[] destPropertyDescriptors = destBeanInfo.getPropertyDescriptors();
		//原始对象
		BeanInfo resBeanInfo = Introspector.getBeanInfo(res.getClass());
		PropertyDescriptor[] resPropertyDescriptors = resBeanInfo.getPropertyDescriptors();
			
		for(PropertyDescriptor resPd : resPropertyDescriptors){
			String resName = resPd.getName();
			Method getMethod = resPd.getReadMethod();
			Object value = getMethod.invoke(res);
			// 不复制class属性，和null值
			if(!"class".equals(resName) && value!=null){
				for(PropertyDescriptor destPd : destPropertyDescriptors){
					if(destPd.getName().equals(resName) && (destPd.getPropertyType()==resPd.getPropertyType())){
						Method setMethod = destPd.getWriteMethod();
						setMethod.invoke(dest, value);
						break;
					}
				}
			}
		}
		return dest;
	}
	
	public static Object copyNotNullString(Object dest, Object res) throws IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		//目标对象
		BeanInfo destBeanInfo = Introspector.getBeanInfo(dest.getClass());
		PropertyDescriptor[] destPropertyDescriptors = destBeanInfo.getPropertyDescriptors();
		//原始对象
		BeanInfo resBeanInfo = Introspector.getBeanInfo(res.getClass());
		PropertyDescriptor[] resPropertyDescriptors = resBeanInfo.getPropertyDescriptors();
			
		for(PropertyDescriptor resPd : resPropertyDescriptors){
			String resName = resPd.getName();
			Method getMethod = resPd.getReadMethod();
			Object value = getMethod.invoke(res);
			// 不复制class属性和null的字符串
			if(!"class".equals(resName) && (value!=null || !resPd.getPropertyType().equals(String.class))){
				for(PropertyDescriptor destPd : destPropertyDescriptors){
					if(destPd.getName().equals(resName) && (destPd.getPropertyType()==resPd.getPropertyType())){
						Method setMethod = destPd.getWriteMethod();
						setMethod.invoke(dest, value);
						break;
					}
				}
			}
		}
		return dest;
	}
	
}
