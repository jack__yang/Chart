package com.zhlq.tips.bean;

public class Tips implements java.io.Serializable{

	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 1L;
	public static final String SUCCESS = "success";
	public static final String FAILURE = "failure";
	public static final String INFO = "info";
	public static final String UNKNOW = "unknow";
	public static final String EXCEPTION = "exception";
	private String info;
	private String url;
	private String type;
	private String label;

	public Tips() {
		super();
	}

	public Tips(String info, String url, String type) {
		super();
		this.info = info;
		this.url = url;
		this.type = type;
	}

	public Tips(String info, String url, String type, String label) {
		super();
		this.info = info;
		this.url = url;
		this.type = type;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "Tips [info=" + info + ", type=" + type + ", url=" + url + "]";
	}

}
