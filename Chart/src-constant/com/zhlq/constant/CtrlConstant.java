package com.zhlq.constant;

import java.util.HashMap;
import java.util.Map;

public final class CtrlConstant {

	public static final String VALIDATOR_RESULT = "ValidatorResult";
	public static final String PAGE_RESULT = "PageResult";
	public static final String PAGE = "Page";
	public static final String QUERY = "Query";
	public static final String UPDATE = "Update";
	public static final String LOOK = "Look";
	public static final String BEAN = "Bean";
	public static final String TIPS = "tips";
	public static final String ADD_SUCCESS = "添加成功";
	public static final String ADD_FAILURE = "添加失败";
	public static final String UPDATE_SUCCESS = "修改成功";
	public static final String OPEN_SUCCESS = "启用成功";
	public static final String CLOSE_SUCCESS = "禁用成功";
	public static final String CLOPEN_SUCCESS = "启用/禁用成功";
	public static final String DELETE_SUCCESS = "删除成功";
	public static final String USER_LIST = "auth/user/list.do";
	public static final String ROLE_LIST = "auth/role/list.do";
	public static final String RESOURCE_TYPE_LIST = "auth/restype/list.do";
	public static final String RESOURCE_LIST = "auth/resource/list.do";
	public static final String FUNCTION_LIST = "auth/function/list.do";
	public static final String TREE_LIST = "auth/tree/list.do";
	public static final String LIST_NULL_EMPTY = "找不到数据";
	public static final String LIST_MULTI = "找到多条数据";
	private static Map<String, String> datas = new HashMap<String, String>();
	
	static {
		datas.put(VALIDATOR_RESULT, "validatorResult");
		datas.put(PAGE_RESULT, "pageResult");
		datas.put(PAGE, "page");
		datas.put(QUERY, "query");
		datas.put(UPDATE, "update");
		datas.put(LOOK, "look");
		datas.put(BEAN, "bean");
		datas.put(TIPS, "tips");
	}

	public static String get(String key) {
		return datas.get(key);
	}

	public static void add(String key, String value) {
		datas.put(key, value);
	}

	private CtrlConstant() {
		super();
	}

}
