<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/tips.css" rel="stylesheet" type="text/css"/>
<table class="ui-list">
	<thead>
		<tr>
			<th></th>
			<th><img src="${ctx }img/operate/table.gif">验证信息</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td>
				<table class="ui-validator">
					<tbody>
						<c:forEach var="validator" items="${validatorResult }">
							<tr>
								<td>&nbsp;</td>
								<td>${validator.chineseName }：</td>
								<td>${validator.tips }</td>
								<td>&nbsp;</td>
							</tr>
						</c:forEach>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><a href="${ctx }${tips.url }">${empty tips.label? '返回' : tips.label }</a></td>
							<td>&nbsp;</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>