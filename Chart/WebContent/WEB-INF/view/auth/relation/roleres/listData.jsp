<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/query.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/operate.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/data.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/component/tree.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/link/link.js" type="text/javascript"></script>
<script src="${ctx }js/list/list.js" type="text/javascript"></script>
<script src="${ctx }js/auth/tree/add.js" type="text/javascript"></script>
<style>
.table-top>th{
	background-image: -webkit-linear-gradient(to top, #CCFF99, #EAFCD5);
	background-image: linear-gradient(to top, #CCFF99, #EAFCD5);
}
.table-bottom>th{
	border-top: 1px solid #9c6;
	background-image: -webkit-linear-gradient(to top, #EAFCD5, #CCFF99);
	background-image: linear-gradient(to top, #EAFCD5, #CCFF99);
}
<%-- 
.ui-auth-current{
	color: red;
}
.ui-auth-current a{
	color: red;
}
 --%>
</style>
<table class="ui-list">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th><img src="${ctx }img/operate/table.gif">编辑角色功能树</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td style="text-align: left;vertical-align: top;">
				<table class="ui-list">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th style="width:150px;text-align: left;">
								<img src="${ctx }img/operate/table.gif">角色功能树
							</th>
							<th style="width:100px;text-align: right;">
								&nbsp;
							</th>
							<th>&nbsp;</th>
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">当前节点</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td rowspan="10"></td>
							<td rowspan="10" colspan="2" class="ui-tree">
								<%-- 树形权限-begin --%>
								<c:set var="first" value="${not empty authTreeList and fn:length(authTreeList) > 0 and not empty authTreeList[0] ? authTreeList[0] : '' }" />
								<c:set var="second" value="${not empty authTreeList and fn:length(authTreeList) > 1 and not empty authTreeList[1] ? authTreeList[1] : '' }" />
								<%-- 第一个节点-begin --%>
								<c:if test="${not empty first }">
									<ul>
									<li class="ui-tree-double ${empty first.id or first.id eq currentNode.id ? 'ui-tree-current' : '' }">
										<%-- 是否选中-begin --%>
										<c:set var="checked" value="0" />
										<c:forEach var="tr" items="${treeRelations }">
											<c:if test="${tr.treeId eq first.id }">
												<c:set var="checked" value="1" />
											</c:if>
										</c:forEach>
										<%-- 是否选中-end --%>
										<input type="checkbox" ${checked eq '1' ? 'checked="checked"' : '' }>
										<a href="javascript:jump('${ctx }auth/tree/roleres/list.do','id=${first.id }');">${first.resource.cname }</a>
									</li>
								</c:if>
								<%-- 第一个节点-end --%>
								<%-- 从第二个节点开始，遍历每个节点-begin --%>
								<c:if test="${not empty authTreeList and fn:length(authTreeList) > 2 }">
									<c:forEach var="indexNode" items="${authTreeList }" begin="2" end="${fn:length(authTreeList)-1 }" step="1">
										<c:if test="${not empty indexNode }">
											<c:if test="${first.level < second.level }">
												<%-- 开始下个子树 --%>
												<ul>
											</c:if>
											<c:if test="${first.level > second.level }">
												<c:forEach begin="${second.level }" end="${first.level-1 }" step="1">
													<%-- 结束当前子树 --%>
													</ul>
												</c:forEach>
											</c:if>
											<li class="${second.id eq currentNode.id ? 'ui-tree-current' : '' } ${first.level<second.level and second.level>indexNode.level?'ui-tree-none':'' }${first.level<second.level?'ui-tree-bottom':'' }${first.level eq second.level and second.level eq indexNode.level?'ui-tree-bottom':'' }${first.level eq second.level and second.level < indexNode.level?'ui-tree-bottom':'' }${first.level eq second.level and second.level > indexNode.level?'ui-tree-none':'' }${first.level>second.level and second.level>indexNode.level?'ui-tree-top':'' }${first.level>second.level and second.level eq indexNode.level?'ui-tree-double':'' }${first.level>second.level and second.level < indexNode.level?'ui-tree-double':'' }">
												<%-- 是否选中-begin --%>
												<c:set var="checked" value="0" />
												<c:forEach var="tr" items="${treeRelations }">
													<c:if test="${tr.treeId eq first.id }">
														<c:set var="checked" value="1" />
													</c:if>
												</c:forEach>
												<%-- 是否选中-end --%>
												<input type="checkbox" ${checked eq '1' ? 'checked="checked"' : '' }>
												<a href="javascript:jump('${ctx }auth/tree/roleres/list.do','id=${second.id }');">${second.resource.cname }</a>
											</li>
											<c:set var="third" value="${first }" />
											<c:set var="first" value="${second }" />
											<c:set var="second" value="${indexNode }" />
										</c:if>
									</c:forEach>
								</c:if>
								<%-- 从第二个节点开始，遍历每个节点-end --%>
								<c:if test="${not empty second }">
									<c:if test="${first.level < second.level }">
										<%-- 开始下个子树 --%>
										<ul>
									</c:if>
									<c:if test="${first.level > second.level }">
										<%-- 结束当前子树 --%>
										</ul>
									</c:if>
									<li class="ui-tree-bottom ${second.id eq currentNode.id ? 'ui-tree-current' : '' }">
										<%-- 是否选中-begin --%>
										<c:set var="checked" value="0" />
										<c:forEach var="tr" items="${treeRelations }">
											<c:if test="${tr.treeId eq first.id }">
												<c:set var="checked" value="1" />
											</c:if>
										</c:forEach>
										<%-- 是否选中-end --%>
										<input type="checkbox" ${checked eq '1' ? 'checked="checked"' : '' }>
										<a href="javascript:jump('${ctx }auth/tree/roleres/list.do','id=${second.id }');">${second.resource.cname }</a>
									</li>
									</ul>
								</c:if>
								<c:if test="${empty second and not empty first }">
									<%-- 结束整颗树 --%>
									</ul>
								</c:if>
								<%-- 树形权限-end --%>
							</td>
							<td rowspan="10"></td>
							<td style="border-left: 1px solid #9c6;"></td>
							<td>
								<table class="ui-data">
									<thead>
										<tr>
											<th>主键</th>
											<th>父主键</th>
											<th>层级</th>
											<th>资源主键</th>
											<th>中文名称</th>
											<th>英文名称</th>
											<th>资源类型</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${empty currentNode }">
											<tr><td colspan="8">暂无</td></tr>
										</c:if>
										<c:if test="${not empty currentNode }">
											<tr>
												<td>${currentNode.id }</td>
												<td>${currentNode.parentId }</td>
												<td>${currentNode.level }</td>
												<td>${currentNode.resId }</td>
												<td>${currentNode.resource.ename }</td>
												<td>${currentNode.resource.cname }</td>
												<td>
													<c:forEach var="resourceType" items="${resourceTypes }">
														${currentNode.resource.type eq resourceType.value ? resourceType.cname : '' }
													</c:forEach>
												</td>
												<td>
													<a href="javascript:jump('${ctx }auth/tree/look.do','id=${currentNode.id }');"><img src="${ctx }img/operate/look.gif">[删除节点]</a>
													<a href="javascript:jump('${ctx }auth/tree/look.do','id=${currentNode.id }');"><img src="${ctx }img/operate/look.gif">[删除子树]</a>
													<a href="javascript:jump('${ctx }auth/tree/look.do','id=${currentNode.id }');"><img src="${ctx }img/operate/look.gif">[强制删除]</a>
												</td>
											</tr>
										</c:if>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
						<tr class="table-top">
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
						
						<!-- 子节点 -->
						<tr class="table-bottom">
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">子节点</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<td style="border-left: 1px solid #9c6;"></td>
							<td style="text-align: left;vertical-align: top;">
								<table class="ui-data">
									<thead>
										<tr>
											<th>主键</th>
											<th>父主键</th>
											<th>层级</th>
											<th>资源主键</th>
											<th>中文名称</th>
											<th>英文名称</th>
											<th>资源类型</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${empty childNodes or fn:length(childNodes)==0 }">
											<tr><td colspan="9">暂无</td></tr>
										</c:if>
										<c:forEach var="item" items="${childNodes }">
											<tr>
												<td>${item.id }</td>
												<td>${item.parentId }</td>
												<td>${item.level }</td>
												<td>${item.resId }</td>
												<td>${item.resource.ename }</td>
												<td>${item.resource.cname }</td>
												<td>
													<c:forEach var="resourceType" items="${resourceTypes }">
														${item.resource.type eq resourceType.value ? resourceType.cname : '' }
													</c:forEach>
												</td>
												<td>
													<a href="javascript:jump('${ctx }auth/relation/function/allot.do','treeId=${item.id }','relationId=${function.id }','currentTreeId=${currentNode.id }');"><img src="${ctx }img/operate/delete.gif">[删除节点]</a>
													<a href="javascript:jump('${ctx }auth/relation/function/allot.do','treeId=${item.id }','relationId=${function.id }','currentTreeId=${currentNode.id }');"><img src="${ctx }img/operate/delete.gif">[删除子树]</a>
													<a href="javascript:jump('${ctx }auth/relation/function/allot.do','treeId=${item.id }','relationId=${function.id }','currentTreeId=${currentNode.id }');"><img src="${ctx }img/operate/delete.gif">[强制删除]</a>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
						<tr class="table-top">
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
						
						<!-- 可选资源节点 -->
						<tr class="table-bottom">
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">可选资源节点</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<td style="border-left: 1px solid #9c6;"></td>
							<td style="text-align: left;vertical-align: top;">
								<form id="queryForm" method="post" action="${ctx }auth/tree/role/query.do">
									<table class="ui-query">
										<tbody>
											<tr>
												<td>
													<%-- 属性名/比较符/值，三者之间不要有空白字符 --%>
													主键:<select name="resourceForm.idCompare">
														<option value="=" ${resourceForm.idCompare eq '='?'selected':'' }>=</option>
														<option value="!=" ${resourceForm.idCompare eq '!='?'selected':'' }>!=</option>
														<option value="&lt;" ${resourceForm.idCompare eq '>'?'selected':'' }>&lt;</option>
														<option value="&lt;=" ${resourceForm.idCompare eq '>='?'selected':'' }>&lt;=</option>
														<option value="&gt;" ${resourceForm.idCompare eq '<'?'selected':'' }>&gt;</option>
														<option value="&gt;=" ${resourceForm.idCompare eq '<='?'selected':'' }>&gt;=</option>
														<option value="in" ${resourceForm.idCompare eq 'in'?'selected':'' }>In</option>
														<option value="not in" ${resourceForm.idCompare eq 'not in'?'selected':'' }>NotIn</option>
														<option value="like" ${resourceForm.idCompare eq 'like'?'selected':'' }>Like</option>
														<option value="not like" ${resourceForm.idCompare eq 'not like'?'selected':'' }>NotLike</option>
														<option value="left like" ${resourceForm.idCompare eq 'left like'?'selected':'' }>LeftLike</option>
														<option value="right like" ${resourceForm.idCompare eq 'right like'?'selected':'' }>RightLike</option>
														<option value="is null" ${resourceForm.idCompare eq 'is null'?'selected':'' }>Null</option>
														<option value="is not null" ${resourceForm.idCompare eq 'is not null'?'selected':'' }>NotNull</option>
														<option value="empty" ${resourceForm.idCompare eq 'empty'?'selected':'' }>Empty</option>
														<option value="not empty" ${resourceForm.idCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
														<option value="null or empty" ${resourceForm.idCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
														<option value="not (null or empty)" ${resourceForm.idCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
													</select><input name="resourceForm.id" value="${resourceForm.id }"/>
												</td>
												<td>
													英文名称:<select name="resourceForm.enameCompare">
														<option value="=" ${resourceForm.enameCompare eq '='?'selected':'' }>=</option>
														<option value="!=" ${resourceForm.enameCompare eq '!='?'selected':'' }>!=</option>
														<option value="&lt;" ${resourceForm.enameCompare eq '>'?'selected':'' }>&lt;</option>
														<option value="&lt;=" ${resourceForm.enameCompare eq '>='?'selected':'' }>&lt;=</option>
														<option value="&gt;" ${resourceForm.enameCompare eq '<'?'selected':'' }>&gt;</option>
														<option value="&gt;=" ${resourceForm.enameCompare eq '<='?'selected':'' }>&gt;=</option>
														<option value="in" ${resourceForm.enameCompare eq 'in'?'selected':'' }>In</option>
														<option value="not in" ${resourceForm.enameCompare eq 'not in'?'selected':'' }>NotIn</option>
														<option value="like" ${resourceForm.enameCompare eq 'like'?'selected':'' }>Like</option>
														<option value="not like" ${resourceForm.enameCompare eq 'not like'?'selected':'' }>NotLike</option>
														<option value="left like" ${resourceForm.enameCompare eq 'left like'?'selected':'' }>LeftLike</option>
														<option value="right like" ${resourceForm.enameCompare eq 'right like'?'selected':'' }>RightLike</option>
														<option value="is null" ${resourceForm.enameCompare eq 'is null'?'selected':'' }>Null</option>
														<option value="is not null" ${resourceForm.enameCompare eq 'is not null'?'selected':'' }>NotNull</option>
														<option value="empty" ${resourceForm.enameCompare eq 'empty'?'selected':'' }>Empty</option>
														<option value="not empty" ${resourceForm.enameCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
														<option value="null or empty" ${resourceForm.enameCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
														<option value="not (null or empty)" ${resourceForm.enameCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
													</select><input name="resourceForm.ename" value="${resourceForm.ename }"/>
												</td>
												<td>
													中文名称:<select name="resourceForm.cnameCompare">
														<option value="=" ${resourceForm.cnameCompare eq '='?'selected':'' }>=</option>
														<option value="!=" ${resourceForm.cnameCompare eq '!='?'selected':'' }>!=</option>
														<option value="&lt;" ${resourceForm.cnameCompare eq '>'?'selected':'' }>&lt;</option>
														<option value="&lt;=" ${resourceForm.cnameCompare eq '>='?'selected':'' }>&lt;=</option>
														<option value="&gt;" ${resourceForm.cnameCompare eq '<'?'selected':'' }>&gt;</option>
														<option value="&gt;=" ${resourceForm.cnameCompare eq '<='?'selected':'' }>&gt;=</option>
														<option value="in" ${resourceForm.cnameCompare eq 'in'?'selected':'' }>In</option>
														<option value="not in" ${resourceForm.cnameCompare eq 'not in'?'selected':'' }>NotIn</option>
														<option value="like" ${resourceForm.cnameCompare eq 'like'?'selected':'' }>Like</option>
														<option value="not like" ${resourceForm.cnameCompare eq 'not like'?'selected':'' }>NotLike</option>
														<option value="left like" ${resourceForm.cnameCompare eq 'left like'?'selected':'' }>LeftLike</option>
														<option value="right like" ${resourceForm.cnameCompare eq 'right like'?'selected':'' }>RightLike</option>
														<option value="is null" ${resourceForm.cnameCompare eq 'is null'?'selected':'' }>Null</option>
														<option value="is not null" ${resourceForm.cnameCompare eq 'is not null'?'selected':'' }>NotNull</option>
														<option value="empty" ${resourceForm.cnameCompare eq 'empty'?'selected':'' }>Empty</option>
														<option value="not empty" ${resourceForm.cnameCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
														<option value="null or empty" ${resourceForm.cnameCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
														<option value="not (null or empty)" ${resourceForm.cnameCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
													</select><input name="resourceForm.cname" value="${resourceForm.cname }"/>
												</td>
												<td>
													类型:<select name="resourceForm.typeCompare">
														<option value="=" ${resourceForm.typeCompare eq '='?'selected':'' }>=</option>
														<option value="!=" ${resourceForm.typeCompare eq '!='?'selected':'' }>!=</option>
														<option value="&lt;" ${resourceForm.typeCompare eq '>'?'selected':'' }>&lt;</option>
														<option value="&lt;=" ${resourceForm.typeCompare eq '>='?'selected':'' }>&lt;=</option>
														<option value="&gt;" ${resourceForm.typeCompare eq '<'?'selected':'' }>&gt;</option>
														<option value="&gt;=" ${resourceForm.typeCompare eq '<='?'selected':'' }>&gt;=</option>
														<option value="in" ${resourceForm.typeCompare eq 'in'?'selected':'' }>In</option>
														<option value="not in" ${resourceForm.typeCompare eq 'not in'?'selected':'' }>NotIn</option>
														<option value="like" ${resourceForm.typeCompare eq 'like'?'selected':'' }>Like</option>
														<option value="not like" ${resourceForm.typeCompare eq 'not like'?'selected':'' }>NotLike</option>
														<option value="left like" ${resourceForm.typeCompare eq 'left like'?'selected':'' }>LeftLike</option>
														<option value="right like" ${resourceForm.typeCompare eq 'right like'?'selected':'' }>RightLike</option>
														<option value="is null" ${resourceForm.typeCompare eq 'is null'?'selected':'' }>Null</option>
														<option value="is not null" ${resourceForm.typeCompare eq 'is not null'?'selected':'' }>NotNull</option>
														<option value="empty" ${resourceForm.typeCompare eq 'empty'?'selected':'' }>Empty</option>
														<option value="not empty" ${resourceForm.typeCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
														<option value="null or empty" ${resourceForm.typeCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
														<option value="not (null or empty)" ${resourceForm.typeCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
													</select><select name="resourceForm.type">
														<option value="">请选择</option>
														<c:forEach var="resourceType" items="${resourceTypes }">
															<option value="${resourceType.value }" ${resourceForm.type eq resourceType.value ? 'selected' : '' }>${resourceType.cname }</option>
														</c:forEach>
													</select>
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td><button>查询</button></td>
											</tr>
										</tbody>
									</table>
								</form>
								<%-- <table class="ui-operate-top">
									<tbody>
										<tr>
											<td>
												<a href="javascript:select();" class="button small green">全选</a>
												<a href="javascript:inverse();" class="button small green">反选</a>
												<a href="javascript:operate({url:'${ctx }auth/tree/role/plus.do',target:'_blank'});" class="button small green">批量添加</a>
											</td>
											<td>
												<input type="button" value="高级查询">
												<button form="${not empty applicationScope.mvc.queryForm ? '' : 'queryForm' }">查询</button>
												<input type="button" value="↓" title="展开所有查询条件">
												<input type="button" value="↑" style="display: none;">
												<input type="button" value="字段" title="字段的选择和排序">
											</td>
										</tr>
									</tbody>
								</table> --%>
								<table class="ui-data">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>主键</th>
											<th>英文名称</th>
											<th>中文名称</th>
											<th>类型</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${empty validateNodeResult.datas }">
											<tr><td colspan="8">暂无</td></tr>
										</c:if>
										<c:forEach var="item" items="${validateNodeResult.datas }">
											<tr>
												<td><input type="checkbox" name="checkbox" data-json="{param:['id','resId'],id:'${currentNode.id }',resId:'${item.id}'}"/></td>
												<td>${item.id }</td>
												<td>${item.ename }</td>
												<td>${item.cname }</td>
												<td>
													<c:forEach var="resourceType" items="${resourceTypes }">
														${item.type eq resourceType.value ? resourceType.cname : '' }
													</c:forEach>
												</td>
												<td>
													<a href="javascript:jump('${ctx }auth/tree/role/plus.do','id=${currentNode.id }','resId=${item.id }');"><img src="${ctx }img/operate/add.gif">[添加]</a>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
								<!-- <table class="ui-operate-middle">
									<tbody>
										<tr><td>
											<a href="javascript:select();" class="button small green">全选</a>
											<a href="javascript:inverse();" class="button small green">反选</a>
										</td></tr>
									</tbody>
								</table> -->
								<table class="ui-operate-bottom">
									<tbody>
										<tr>
											<td>
												<jsp:include page="/WEB-INF/component/page.jsp">
													<jsp:param value="${validateNodeResult.page }" name="page"/>
												</jsp:include>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th></th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th></th>
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>