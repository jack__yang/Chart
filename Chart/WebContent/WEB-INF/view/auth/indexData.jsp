<%@ page pageEncoding="UTF-8"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<table class="ui-list">
	<thead>
		<tr>
			<th></th>
			<th><img src="${ctx }img/operate/table.gif">XXX管理</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td>
				<form action="">
					<table class="ui-query">
						<tbody>
							<tr>
								<td>表头1:</td>
								<td><input/></td>
								<td>表头2:</td>
								<td><input/></td>
								<td>表头3:</td>
								<td><input/></td>
								<td>表头4:</td>
								<td><input/></td>
							</tr>
							<tr>
								<td>21:</td>
								<td><input/></td>
								<td>22:</td>
								<td><input/></td>
								<td>23:</td>
								<td><input/></td>
								<td>24:</td>
								<td><input/></td>
							</tr>
							<tr>
								<td colspan="7">&nbsp;</td>
								<td><input type="submit" value="提交"/></td>
							</tr>
						</tbody>
					</table>
				</form>
				<table class="ui-operate-top">
					<tbody>
						<tr>
							<td>
								<a class="button small green">全选</a>
								<a class="button small green">反选</a>
								<a class="button small green">批量禁用</a>
								<a class="button small green">批量删除</a>
							</td>
							<td>
								<a href="${ctx }view/base/add.jsp"><img src="${ctx }img/operate/add.gif">[新增]</a>
							</td>
						</tr>
					</tbody>
				</table>
				<table class="ui-data">
					<thead>
						<tr>
							<th><input type="checkbox"/></th>
							<th>表头1</th>
							<th>表头2</th>
							<th>表头3</th>
							<th>表头4</th>
							<th>表头5</th>
							<th>表头6</th>
							<th>表头7</th>
							<th>表头8</th>
							<th>表头9</th>
							<th>操作</th>
						</tr>
					</thead>
					<tr>
						<td><input type="checkbox"/></td>
						<td>0101</td>
						<td>10</td>
						<td>20</td>
						<td></td>
						<td>70</td>
						<td>20</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>
							<a href="${ctx }view/base/look.jsp"><img src="${ctx }img/operate/look.gif">[查看]</a>
							<a href="${ctx }view/base/edit.jsp"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
							<a href="${ctx }view/base/open.jsp"><span class="ui-operate-open">&nbsp;</span>[启用]</a>
							<a href="${ctx }view/base/delete.jsp"><img src="${ctx }img/operate/delete.gif">[删除]</a>
						</td>
					</tr>
					<tr>
						<td><input type="checkbox"/></td>
						<td>0101</td>
						<td>10</td>
						<td>20</td>
						<td></td>
						<td>70</td>
						<td>20</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>
							<a href="${ctx }view/base/look.jsp"><img src="${ctx }img/operate/look.gif">[查看]</a>
							<a href="${ctx }view/base/edit.jsp"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
							<a href="${ctx }view/base/close.jsp"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
							<a href="${ctx }view/base/delete.jsp"><img src="${ctx }img/operate/delete.gif">[删除]</a>
						</td>
					</tr>
					<tr>
						<td><input type="checkbox"/></td>
						<td>0101</td>
						<td>10</td>
						<td>20</td>
						<td></td>
						<td>70</td>
						<td>20</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>
							<a href="${ctx }view/base/look.jsp"><img src="${ctx }img/operate/look.gif">[查看]</a>
							<a href="${ctx }view/base/edit.jsp"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
							<a href="${ctx }view/base/close.jsp"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
							<a href="${ctx }view/base/delete.jsp"><img src="${ctx }img/operate/delete.gif">[删除]</a>
						</td>
					</tr>
					<tr>
						<td><input type="checkbox"/></td>
						<td>0101</td>
						<td>10</td>
						<td>20</td>
						<td></td>
						<td>70</td>
						<td>20</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>
							<a href="${ctx }view/base/look.jsp"><img src="${ctx }img/operate/look.gif">[查看]</a>
							<a href="${ctx }view/base/edit.jsp"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
							<a href="${ctx }view/base/close.jsp"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
							<a href="${ctx }view/base/delete.jsp"><img src="${ctx }img/operate/delete.gif">[删除]</a>
						</td>
					</tr>
					<tr>
						<td><input type="checkbox"/></td>
						<td>0101</td>
						<td>10</td>
						<td>20</td>
						<td></td>
						<td>70</td>
						<td>20</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>
							<a href="${ctx }view/base/look.jsp"><img src="${ctx }img/operate/look.gif">[查看]</a>
							<a href="${ctx }view/base/edit.jsp"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
							<a href="${ctx }view/base/close.jsp"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
							<a href="${ctx }view/base/delete.jsp"><img src="${ctx }img/operate/delete.gif">[删除]</a>
						</td>
					</tr>
					<tr>
						<td><input type="checkbox"/></td>
						<td>0102</td>
						<td>12</td>
						<td>34</td>
						<td>55</td>
						<td>555</td>
						<td>55</td>
						<td></td>
						<td></td>
						<td></td>
						<td>
							<a href="${ctx }view/base/look.jsp"><img src="${ctx }img/operate/look.gif">[查看]</a>
							<a href="${ctx }view/base/edit.jsp"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
							<a href="${ctx }view/base/close.jsp"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
							<a href="${ctx }view/base/delete.jsp"><img src="${ctx }img/operate/delete.gif">[删除]</a>
						</td>
					</tr>
					<tr>
						<td><input type="checkbox"/></td>
						<td>0150</td>
						<td>310000</td>
						<td>310000</td>
						<td>0</td>
						<td>3000</td>
						<td>2923</td>
						<td>400</td>
						<td>391</td>
						<td>219</td>
						<td>
							<a href="${ctx }view/base/look.jsp"><img src="${ctx }img/operate/look.gif">[查看]</a>
							<a href="${ctx }view/base/edit.jsp"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
							<a href="${ctx }view/base/close.jsp"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
							<a href="${ctx }view/base/delete.jsp"><img src="${ctx }img/operate/delete.gif">[删除]</a>
						</td>
					</tr>
					<tr>
						<td><input type="checkbox"/></td>
						<td>0152</td>
						<td>305250</td>
						<td>0</td>
						<td>-1</td>
						<td>2493</td>
						<td>2056</td>
						<td>121</td>
						<td>121</td>
						<td>14</td>
						<td>
							<a href="${ctx }view/base/look.jsp"><img src="${ctx }img/operate/look.gif">[查看]</a>
							<a href="${ctx }view/base/edit.jsp"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
							<a href="${ctx }view/base/close.jsp"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
							<a href="${ctx }view/base/delete.jsp"><img src="${ctx }img/operate/delete.gif">[删除]</a>
						</td>
					</tr>
					<tr>
						<td><input type="checkbox"/></td>
						<td>0153</td>
						<td>764800</td>
						<td>719107</td>
						<td>0</td>
						<td>6078</td>
						<td>5751</td>
						<td>209</td>
						<td>225</td>
						<td>14</td>
						<td>
							<a href="${ctx }view/base/look.jsp"><img src="${ctx }img/operate/look.gif">[查看]</a>
							<a href="${ctx }view/base/edit.jsp"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
							<a href="${ctx }view/base/close.jsp"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
							<a href="${ctx }view/base/delete.jsp"><img src="${ctx }img/operate/delete.gif">[删除]</a>
						</td>
					</tr>
					<tr>
						<td><input type="checkbox"/></td>
						<td>0167</td>
						<td>236000</td>
						<td>54360</td>
						<td>0</td>
						<td>8647</td>
						<td>10105</td>
						<td>258</td>
						<td>320</td>
						<td>78</td>
						<td>
							<a href="${ctx }view/base/look.jsp"><img src="${ctx }img/operate/look.gif">[查看]</a>
							<a href="${ctx }view/base/edit.jsp"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
							<a href="${ctx }view/base/close.jsp"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
							<a href="${ctx }view/base/delete.jsp"><img src="${ctx }img/operate/delete.gif">[删除]</a>
						</td>
					</tr>
					<tr>
						<td><input type="checkbox"/></td>
						<td>0167</td>
						<td>236000</td>
						<td>54360</td>
						<td>0</td>
						<td>8647</td>
						<td>10105</td>
						<td>258</td>
						<td>320</td>
						<td>78</td>
						<td>
							<a href="${ctx }view/base/look.jsp"><img src="${ctx }img/operate/look.gif">[查看]</a>
							<a href="${ctx }view/base/edit.jsp"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
							<a href="${ctx }view/base/${ctx }view/base/close.jsp"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
							<a href="${ctx }view/base/delete.jsp"><img src="${ctx }img/operate/delete.gif">[删除]</a>
						</td>
					</tr>
					<tr>
						<td><input type="checkbox"/></td>
						<td>0167</td>
						<td>236000</td>
						<td>54360</td>
						<td>0</td>
						<td>8647</td>
						<td>10105</td>
						<td>258</td>
						<td>320</td>
						<td>78</td>
						<td>
							<a href="${ctx }view/base/look.jsp"><img src="${ctx }img/operate/look.gif">[查看]</a>
							<a href="${ctx }view/base/edit.jsp"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
							<a href="${ctx }view/base/close.jsp"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
							<a href="${ctx }view/base/delete.jsp"><img src="${ctx }img/operate/delete.gif">[删除]</a>
						</td>
					</tr>
					<tr>
						<td><input type="checkbox"/></td>
						<td>0167</td>
						<td>236000</td>
						<td>54360</td>
						<td>0</td>
						<td>8647</td>
						<td>10105</td>
						<td>258</td>
						<td>320</td>
						<td>78</td>
						<td>
							<a href="${ctx }view/base/look.jsp"><img src="${ctx }img/operate/look.gif">[查看]</a>
							<a href="${ctx }view/base/edit.jsp"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
							<a href="${ctx }view/base/close.jsp"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
							<a href="${ctx }view/base/delete.jsp"><img src="${ctx }img/operate/delete.gif">[删除]</a>
						</td>
					</tr>
					<tr>
						<td><input type="checkbox"/></td>
						<td>0167</td>
						<td>236000</td>
						<td>54360</td>
						<td>0</td>
						<td>8647</td>
						<td>10105</td>
						<td>258</td>
						<td>320</td>
						<td>78</td>
						<td>
							<a href="${ctx }view/base/look.jsp"><img src="${ctx }img/operate/look.gif">[查看]</a>
							<a href="${ctx }view/base/edit.jsp"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
							<a href="${ctx }view/base/close.jsp"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
							<a href="${ctx }view/base/delete.jsp"><img src="${ctx }img/operate/delete.gif">[删除]</a>
						</td>
					</tr>
					<tr>
						<td><input type="checkbox"/></td>
						<td>0167</td>
						<td>236000</td>
						<td>54360</td>
						<td>0</td>
						<td>8647</td>
						<td>10105</td>
						<td>258</td>
						<td>320</td>
						<td>78</td>
						<td>
							<a href="${ctx }view/base/look.jsp"><img src="${ctx }img/operate/look.gif">[查看]</a>
							<a href="${ctx }view/base/edit.jsp"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
							<a href="${ctx }view/base/close.jsp"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
							<a href="${ctx }view/base/delete.jsp"><img src="${ctx }img/operate/delete.gif">[删除]</a>
						</td>
					</tr>
				</table>
				<table class="ui-operate-middle">
					<tbody>
						<tr><td>
							<a class="button small green">全选</a>
							<a class="button small green">反选</a>
							<a class="button small green">批量禁用</a>
							<a class="button small green">批量删除</a>
						</td></tr>
					</tbody>
				</table>
				<table class="ui-operate-bottom">
					<tbody>
						<tr>
							<td><%@include file="/WEB-INF/component/page.jsp"%></td>
						</tr>
					</tbody>
				</table>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>