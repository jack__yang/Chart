<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/data.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/link/link.js" type="text/javascript"></script>
<script src="${ctx }js/auth/tree/add.js" type="text/javascript"></script>
<style>
ul{
	list-style:none;
}
li{
	border-left: 1px solid #9c6;
	border-top: 1px solid #9c6;
}
.ui-list>tbody>tr>td>ul>li:nth-child(1){
	border-right: 1px solid #9c6;
	border-bottom: 1px solid #9c6;
}
.table-top>th{
	background-image: -webkit-linear-gradient(to top, #CCFF99, #EAFCD5);
	background-image: linear-gradient(to top, #CCFF99, #EAFCD5);
}
.table-bottom>th{
	border-top: 1px solid #9c6;
	background-image: -webkit-linear-gradient(to top, #EAFCD5, #CCFF99);
	background-image: linear-gradient(to top, #EAFCD5, #CCFF99);
}
.ui-auth-current{
	color: red;
}
.ui-auth-current a{
	color: red;
}
</style>
<table class="ui-list">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th><img src="${ctx }img/operate/table.gif">编辑资源树</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td style="text-align: left;vertical-align: top;">
				<table class="ui-list">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th style="width:100px;text-align: left;">
								<img src="${ctx }img/operate/table.gif">资源树
							</th>
							<th style="width:100px;text-align: right;">
								<a href="javascript:jump('${ctx }auth/tree/delete.do','id=${item.id }');"><img src="${ctx }img/operate/delete.gif">[批量删除]</a>
							</th>
							<th>&nbsp;</th>
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">当前节点</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td rowspan="7"></td>
							<td rowspan="7" colspan="2" style="text-align: left;vertical-align: top;">
								${tree }
							</td>
							<td rowspan="7"></td>
							<td style="border-left: 1px solid #9c6;"></td>
							<td style="text-align: left;vertical-align: top;">
								<table class="ui-data">
									<thead>
										<tr>
											<th><input type="checkbox"/></th>
											<th>主键</th>
											<th>父主键</th>
											<th>层级</th>
											<th>资源主键</th>
											<th>角色主键</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><input type="checkbox"/></td>
											<td>${currentNode.id }</td>
											<td>${currentNode.parentId }</td>
											<td>${currentNode.level }</td>
											<td>${currentNode.resId }</td>
											<td>${currentNode.roleId }</td>
											<td>
												<a href="javascript:jump('${ctx }auth/tree/delete.do','id=${currentNode.id }');"><img src="${ctx }img/operate/delete.gif">[删除]</a>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
						<tr class="table-top">
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
						<tr class="table-bottom">
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">子节点</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<td style="border-left: 1px solid #9c6;"></td>
							<td>
								<form id="addForm" method="post" action="${ctx }auth/tree/resaddtree.do">
									<table style="width: 100%;">
										<tbody>
											<tr>
												<td>
													英文名称:<input name="resource.ename"/>
												</td>
												<td>
													中文名称:<input name="resource.cname"/>
												</td>
												<td>
													类型:<select name="resource.type" style="width: 155px;">
															<option value="">请选择</option>
															<c:forEach var="resourceType" items="${resourceTypes }">
																<option value="${resourceType.value }">${resourceType.cname }</option>
															</c:forEach>
														</select>
												</td>
												<td>
													<input type="hidden" name="id" value="${currentNode.id }" >
													<input type="button" value="新增" onclick="add()"/>
												</td>
											</tr>
										</tbody>
									</table>
								</form>
								<table class="ui-data">
									<thead>
										<tr>
											<th><input type="checkbox"/></th>
											<th>主键</th>
											<th>父主键</th>
											<th>层级</th>
											<th>资源主键</th>
											<th>角色主键</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="item" items="${childNodes }">
											<tr>
												<td><input type="checkbox"/></td>
												<td>${item.id }</td>
												<td>${item.parentId }</td>
												<td>${item.level }</td>
												<td>${item.resId }</td>
												<td>${item.roleId }</td>
												<%-- <td>
													<c:forEach var="resourceType" items="${resourceTypes }">
														${item.type eq resourceType.value ? resourceType.cname : '' }
													</c:forEach>
												</td> --%>
												<td>
													<a href="javascript:jump('${ctx }auth/tree/delete.do','id=${item.id }');"><img src="${ctx }img/operate/delete.gif">[删除]</a>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
						<tr class="table-top">
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
						<tr class="table-bottom">
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">可选节点</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<td style="border-left: 1px solid #9c6;"></td>
							<td style="text-align: left;vertical-align: top;">
								<table class="ui-data">
									<thead>
										<tr>
											<th><input type="checkbox"/></th>
											<th>主键</th>
											<th>英文名称</th>
											<th>中文名称</th>
											<th>类型</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="item" items="${resources }">
											<tr>
												<td><input type="checkbox"/></td>
												<td>${item.id }</td>
												<td>${item.ename }</td>
												<td>${item.cname }</td>
												<td>
													<c:forEach var="resourceType" items="${resourceTypes }">
														${item.type eq resourceType.value ? resourceType.cname : '' }
													</c:forEach>
												</td>
												<td>
													<a href="javascript:jump('${ctx }auth/tree/res2tree.do','id=${currentNode.id }','resId=${item.id }');"><img src="${ctx }img/operate/add.gif">[添加]</a>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th></th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th></th>
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>