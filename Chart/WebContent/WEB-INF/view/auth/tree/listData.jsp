<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/query.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/operate.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/data.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/link/link.js" type="text/javascript"></script>
<table class="ui-list">
	<thead>
		<tr>
			<th></th>
			<th><img src="${ctx }img/operate/table.gif">资源树列表</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td>
				<form method="post" action="${ctx }auth/tree/list.do">
					<table class="ui-query">
						<tbody>
							<tr>
								<td>
									<%-- 属性名/比较符/值，三者之间不要有空白字符 --%>
									主键:<select name="idCompare">
										<option value="=" ${query.idCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.idCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.idCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.idCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.idCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.idCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.idCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.idCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.idCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.idCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.idCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.idCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.idCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.idCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.idCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.idCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.idCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.idCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="id" value="${query.id }"/>
								</td>
								<td>
									父主键:<select name="parentIdCompare">
										<option value="=" ${query.parentIdCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.parentIdCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.parentIdCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.parentIdCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.parentIdCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.parentIdCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.parentIdCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.parentIdCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.parentIdCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.parentIdCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.parentIdCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.parentIdCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.parentIdCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.parentIdCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.parentIdCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.parentIdCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.parentIdCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.parentIdCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="parentId" value="${query.parentId }"/>
								</td>
								<td>
									深度:<select name="levelCompare">
										<option value="=" ${query.levelCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.levelCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.levelCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.levelCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.levelCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.levelCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.levelCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.levelCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.levelCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.levelCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.levelCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.levelCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.levelCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.levelCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.levelCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.levelCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.levelCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.levelCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="level" value="${query.level }"/>
								</td>
								<td>
									排序:<select name="sortCompare">
										<option value="=" ${query.sortCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.sortCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.sortCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.sortCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.sortCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.sortCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.sortCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.sortCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.sortCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.sortCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.sortCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.sortCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.sortCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.sortCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.sortCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.sortCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.sortCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.sortCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="sort" value="${query.sort }"/>
								</td>
							</tr>
							<tr>
								<td>
									资源主键:<select name="resIdCompare">
										<option value="=" ${query.resIdCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.resIdCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.resIdCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.resIdCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.resIdCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.resIdCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.resIdCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.resIdCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.resIdCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.resIdCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.resIdCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.resIdCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.resIdCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.resIdCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.resIdCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.resIdCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.resIdCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.resIdCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="resId" value="${query.resId }"/>
								</td>
								<td>
									树名:<select name="nameCompare">
										<option value="=" ${query.nameCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.nameCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.nameCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.nameCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.nameCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.nameCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.nameCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.nameCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.nameCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.nameCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.nameCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.nameCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.nameCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.nameCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.nameCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.nameCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.nameCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.nameCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="name" value="${query.name }"/>
								</td>
								<td>&nbsp;
								</td>
								<td><button>查询</button></td>
							</tr>
						</tbody>
					</table>
				</form>
				<table class="ui-operate-top">
					<tbody>
						<tr>
							<td>
								<a class="button small green">全选</a>
								<a class="button small green">反选</a>
								<a class="button small green">批量禁用</a>
								<a class="button small green">批量删除</a>
							</td>
							<td>
								<a href="${ctx }auth/tree/toadd.do"><img src="${ctx }img/operate/add.gif">[导出]</a>
								<a href="${ctx }auth/tree/toadd.do"><img src="${ctx }img/operate/add.gif">[导入]</a>
								<a href="${ctx }auth/tree/toadd.do"><img src="${ctx }img/operate/add.gif">[新增]</a>
							</td>
						</tr>
					</tbody>
				</table>
				<table class="ui-data">
					<thead>
						<tr>
							<th><input type="checkbox"/></th>
							<th>主键</th>
							<th>父主键</th>
							<th>深度</th>
							<th>排序</th>
							<th>资源主键</th>
							<th>树名</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${pageResult.datas }">
							<tr>
								<td><input type="checkbox"/></td>
								<td>${item.id }</td>
								<td>${item.parentId }</td>
								<td>${item.level }</td>
								<td>${item.sort }</td>
								<td>${item.resId }</td>
								<td>${item.name }</td>
								<td>
									<a href="javascript:jump('${ctx }auth/tree/look.do','id=${item.id }');"><img src="${ctx }img/operate/look.gif">[查看]</a>
									<a href="javascript:jump('${ctx }auth/tree/toedit.do','id=${item.id }');"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
									<a href="javascript:jump('${ctx }auth/tree/delete.do','id=${item.id }');"><img src="${ctx }img/operate/delete.gif">[删除]</a>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<table class="ui-operate-middle">
					<tbody>
						<tr><td>
							<a class="button small green">全选</a>
							<a class="button small green">反选</a>
							<a class="button small green">批量禁用</a>
							<a class="button small green">批量删除</a>
						</td></tr>
					</tbody>
				</table>
				<table class="ui-operate-bottom">
					<tbody>
						<tr>
							<td><%@include file="/WEB-INF/component/page.jsp"%></td>
						</tr>
					</tbody>
				</table>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>