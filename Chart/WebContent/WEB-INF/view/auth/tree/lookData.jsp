<%@ page pageEncoding="UTF-8"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/add2.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/link/link.js" type="text/javascript"></script>
<table class="ui-list">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th><img src="${ctx }img/operate/table.gif">查看资源树</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td>
				<form action="">
					<table class="ui-add">
						<tbody>
							<tr>
								<td>父主键:</td>
								<td>${look.parentId }</td>
							</tr>
							<tr>
								<td>深度:</td>
								<td>${look.level }</td>
							</tr>
							<tr>
								<td>排序:</td>
								<td>
									${look.sort }
								</td>
							</tr>
							<tr>
								<td>资源主键:</td>
								<td>
									${look.resId }
								</td>
							</tr>
							<tr>
								<td>树名:</td>
								<td>
									${look.name }
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<input type="button" value="返回" onclick="back()"/>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>

<script>
	$(document).ready(function() { $(".ui-add").colorize(); });
</script>