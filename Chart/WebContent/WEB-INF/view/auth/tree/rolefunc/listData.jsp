<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/query.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/operate.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/data.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/component/tree.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/link/link.js" type="text/javascript"></script>
<script src="${ctx }js/list/list.js" type="text/javascript"></script>
<script src="${ctx }js/auth/tree/add.js" type="text/javascript"></script>
<style>
.table-top>th{
	background-image: -webkit-linear-gradient(to top, #CCFF99, #EAFCD5);
	background-image: linear-gradient(to top, #CCFF99, #EAFCD5);
}
.table-bottom>th{
	border-top: 1px solid #9c6;
	background-image: -webkit-linear-gradient(to top, #EAFCD5, #CCFF99);
	background-image: linear-gradient(to top, #EAFCD5, #CCFF99);
}
<%-- 
.ui-auth-current{
	color: red;
}
.ui-auth-current a{
	color: red;
}
 --%>
</style>
<table class="ui-list">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th><img src="${ctx }img/operate/table.gif">编辑角色功能树</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td style="text-align: left;vertical-align: top;">
				<table class="ui-list">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th style="width:150px;text-align: left;">
								<img src="${ctx }img/operate/table.gif">角色功能树
							</th>
							<th style="width:100px;text-align: right;">
								&nbsp;
							</th>
							<th>&nbsp;</th>
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">当前节点</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td rowspan="10"></td>
							<td rowspan="10" colspan="2" class="ui-tree">
								<%-- 树形权限-begin --%>
								<c:set var="first" value="${not empty authTreeList and fn:length(authTreeList) > 0 and not empty authTreeList[0] ? authTreeList[0] : '' }" />
								<c:set var="second" value="${not empty authTreeList and fn:length(authTreeList) > 1 and not empty authTreeList[1] ? authTreeList[1] : '' }" />
								<%-- 第一个节点-begin --%>
								<c:if test="${not empty first }">
									<ul>
									<li class="ui-tree-double ${empty first.id or first.id eq currentNode.id ? 'ui-tree-current' : '' }">
										<%-- 是否选中-begin --%>
										<c:set var="checked" value="0" />
										<c:forEach var="tr" items="${treeRelations }">
											<c:if test="${tr.treeId eq first.id }">
												<c:set var="checked" value="1" />
											</c:if>
										</c:forEach>
										<%-- 是否选中-end --%>
										<input type="checkbox" ${checked eq '1' ? 'checked="checked"' : '' }>
										<a href="javascript:jump('${ctx }auth/tree/rolefunc/list.do','id=${first.id }');">${first.name eq 'ROLE_FUNC'?first.function.cname:first.resource.cname }</a>
									</li>
								</c:if>
								<%-- 第一个节点-end --%>
								<%-- 从第二个节点开始，遍历每个节点-begin --%>
								<c:if test="${not empty authTreeList and fn:length(authTreeList) > 2 }">
									<c:forEach var="indexNode" items="${authTreeList }" begin="2" end="${fn:length(authTreeList)-1 }" step="1">
										<c:if test="${not empty indexNode }">
											<c:if test="${first.level < second.level }">
												<%-- 开始下个子树 --%>
												<ul>
											</c:if>
											<c:if test="${first.level > second.level }">
												<c:forEach begin="${second.level }" end="${first.level-1 }" step="1">
													<%-- 结束当前子树 --%>
													</ul>
												</c:forEach>
											</c:if>
											<li class="${second.id eq currentNode.id ? 'ui-tree-current' : '' } ${first.level<second.level and second.level>indexNode.level?'ui-tree-none':'' }${first.level<second.level?'ui-tree-bottom':'' }${first.level eq second.level and second.level eq indexNode.level?'ui-tree-bottom':'' }${first.level eq second.level and second.level < indexNode.level?'ui-tree-bottom':'' }${first.level eq second.level and second.level > indexNode.level?'ui-tree-none':'' }${first.level>second.level and second.level>indexNode.level?'ui-tree-top':'' }${first.level>second.level and second.level eq indexNode.level?'ui-tree-double':'' }${first.level>second.level and second.level < indexNode.level?'ui-tree-double':'' }">
												<%-- 是否选中-begin --%>
												<c:set var="checked" value="0" />
												<c:forEach var="tr" items="${treeRelations }">
													<c:if test="${tr.treeId eq first.id }">
														<c:set var="checked" value="1" />
													</c:if>
												</c:forEach>
												<%-- 是否选中-end --%>
												<input type="checkbox" ${checked eq '1' ? 'checked="checked"' : '' }>
												<a href="javascript:jump('${ctx }auth/tree/rolefunc/list.do','id=${second.id }');">${second.name eq 'ROLE_FUNC'?second.function.cname:second.resource.cname }</a>
											</li>
											<c:set var="third" value="${first }" />
											<c:set var="first" value="${second }" />
											<c:set var="second" value="${indexNode }" />
										</c:if>
									</c:forEach>
								</c:if>
								<%-- 从第二个节点开始，遍历每个节点-end --%>
								<c:if test="${not empty second }">
									<c:if test="${first.level < second.level }">
										<%-- 开始下个子树 --%>
										<ul>
									</c:if>
									<c:if test="${first.level > second.level }">
										<%-- 结束当前子树 --%>
										</ul>
									</c:if>
									<li class="ui-tree-bottom ${second.id eq currentNode.id ? 'ui-tree-current' : '' }">
										<%-- 是否选中-begin --%>
										<c:set var="checked" value="0" />
										<c:forEach var="tr" items="${treeRelations }">
											<c:if test="${tr.treeId eq first.id }">
												<c:set var="checked" value="1" />
											</c:if>
										</c:forEach>
										<%-- 是否选中-end --%>
										<input type="checkbox" ${checked eq '1' ? 'checked="checked"' : '' }>
										<a href="javascript:jump('${ctx }auth/tree/rolefunc/list.do','id=${second.id }');">${second.name eq 'ROLE_FUNC'?second.function.cname:second.resource.cname }</a>
									</li>
									</ul>
								</c:if>
								<c:if test="${empty second and not empty first }">
									<%-- 结束整颗树 --%>
									</ul>
								</c:if>
								<%-- 树形权限-end --%>
							</td>
							<td rowspan="10"></td>
							<td style="border-left: 1px solid #9c6;"></td>
							<td>
								<table class="ui-data">
									<thead>
										<tr>
											<th>主键</th>
											<th>父主键</th>
											<th>层级</th>
											<th>资源主键</th>
											<th>英文名称</th>
											<th>中文名称</th>
											<th>资源类型</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${empty currentNode }">
											<tr><td colspan="8">暂无</td></tr>
										</c:if>
										<c:if test="${not empty currentNode }">
											<tr>
												<td>${currentNode.id }</td>
												<td>${currentNode.parentId }</td>
												<td>${currentNode.level }</td>
												<td>${currentNode.resId }</td>
												<td>${currentNode.resource.ename }</td>
												<td>${currentNode.resource.cname }</td>
												<td>
													<c:forEach var="resourceType" items="${resourceTypes }">
														${currentNode.resource.type eq resourceType.value ? resourceType.cname : '' }
													</c:forEach>
												</td>
												<td>
													<a href="javascript:jump('${ctx }auth/tree/look.do','id=${currentNode.id }');"><img src="${ctx }img/operate/look.gif">[删除节点]</a>
													<a href="javascript:jump('${ctx }auth/tree/look.do','id=${currentNode.id }');"><img src="${ctx }img/operate/look.gif">[删除子树]</a>
													<a href="javascript:jump('${ctx }auth/tree/look.do','id=${currentNode.id }');"><img src="${ctx }img/operate/look.gif">[强制删除]</a>
												</td>
											</tr>
										</c:if>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
						<tr class="table-top">
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
						
						<!-- 子资源节点 -->
						<tr class="table-bottom">
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">子资源节点</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<td style="border-left: 1px solid #9c6;"></td>
							<td style="text-align: left;vertical-align: top;">
								<table class="ui-data">
									<thead>
										<tr>
											<th>主键</th>
											<th>父主键</th>
											<th>层级</th>
											<th>资源主键</th>
											<th>英文名称</th>
											<th>中文名称</th>
											<th>资源类型</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${empty childNodes or fn:length(childNodes)==0 }">
											<tr><td colspan="9">暂无</td></tr>
										</c:if>
										<c:forEach var="item" items="${childNodes }">
											<tr>
												<td>${item.id }</td>
												<td>${item.parentId }</td>
												<td>${item.level }</td>
												<td>${item.resId }</td>
												<td>${item.resource.ename }</td>
												<td>${item.resource.cname }</td>
												<td>
													<c:forEach var="resourceType" items="${resourceTypes }">
														${item.resource.type eq resourceType.value ? resourceType.cname : '' }
													</c:forEach>
												</td>
												<td>
													<a href="javascript:jump('${ctx }auth/relation/function/allot.do','treeId=${item.id }','relationId=${function.id }','currentTreeId=${currentNode.id }');"><img src="${ctx }img/operate/delete.gif">[删除节点]</a>
													<a href="javascript:jump('${ctx }auth/relation/function/allot.do','treeId=${item.id }','relationId=${function.id }','currentTreeId=${currentNode.id }');"><img src="${ctx }img/operate/delete.gif">[删除子树]</a>
													<a href="javascript:jump('${ctx }auth/relation/function/allot.do','treeId=${item.id }','relationId=${function.id }','currentTreeId=${currentNode.id }');"><img src="${ctx }img/operate/delete.gif">[强制删除]</a>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
						<tr class="table-top">
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
						
						<!-- 子功能节点 -->
						<tr class="table-bottom">
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">子功能节点</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<td style="border-left: 1px solid #9c6;"></td>
							<td style="text-align: left;vertical-align: top;">
								<table class="ui-data">
									<thead>
										<tr>
											<th>主键</th>
											<th>父主键</th>
											<th>层级</th>
											<th>资源主键</th>
											<th>中文名称</th>
											<th>英文名称</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${empty childFuncNodes or fn:length(childFuncNodes)==0 }">
											<tr><td colspan="9">暂无</td></tr>
										</c:if>
										<c:forEach var="item" items="${childFuncNodes }">
											<tr>
												<td>${item.id }</td>
												<td>${item.parentId }</td>
												<td>${item.level }</td>
												<td>${item.resId }</td>
												<td>${item.function.ename }</td>
												<td>${item.function.cname }</td>
												<td>
													<a href="javascript:jump('${ctx }auth/relation/function/allot.do','treeId=${item.id }','relationId=${function.id }','currentTreeId=${currentNode.id }');"><img src="${ctx }img/operate/delete.gif">[删除节点]</a>
													<a href="javascript:jump('${ctx }auth/relation/function/allot.do','treeId=${item.id }','relationId=${function.id }','currentTreeId=${currentNode.id }');"><img src="${ctx }img/operate/delete.gif">[删除子树]</a>
													<a href="javascript:jump('${ctx }auth/relation/function/allot.do','treeId=${item.id }','relationId=${function.id }','currentTreeId=${currentNode.id }');"><img src="${ctx }img/operate/delete.gif">[强制删除]</a>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
						<tr class="table-top">
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
						
						<!-- 可选资源节点 -->
						<tr class="table-bottom">
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">可选功能节点</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<td style="border-left: 1px solid #9c6;"></td>
							<td style="text-align: left;vertical-align: top;">
								<form method="post" action="${ctx }auth/function/list.do">
									<table class="ui-query">
										<tbody>
											<tr>
												<td>
													<%-- 属性名/比较符/值，三者之间不要有空白字符 --%>
													主键:<select name="idCompare">
														<option value="=" ${query.idCompare eq '='?'selected':'' }>=</option>
														<option value="!=" ${query.idCompare eq '!='?'selected':'' }>!=</option>
														<option value="&lt;" ${query.idCompare eq '>'?'selected':'' }>&lt;</option>
														<option value="&lt;=" ${query.idCompare eq '>='?'selected':'' }>&lt;=</option>
														<option value="&gt;" ${query.idCompare eq '<'?'selected':'' }>&gt;</option>
														<option value="&gt;=" ${query.idCompare eq '<='?'selected':'' }>&gt;=</option>
														<option value="in" ${query.idCompare eq 'in'?'selected':'' }>In</option>
														<option value="not in" ${query.idCompare eq 'not in'?'selected':'' }>NotIn</option>
														<option value="like" ${query.idCompare eq 'like'?'selected':'' }>Like</option>
														<option value="not like" ${query.idCompare eq 'not like'?'selected':'' }>NotLike</option>
														<option value="left like" ${query.idCompare eq 'left like'?'selected':'' }>LeftLike</option>
														<option value="right like" ${query.idCompare eq 'right like'?'selected':'' }>RightLike</option>
														<option value="is null" ${query.idCompare eq 'is null'?'selected':'' }>Null</option>
														<option value="is not null" ${query.idCompare eq 'is not null'?'selected':'' }>NotNull</option>
														<option value="empty" ${query.idCompare eq 'empty'?'selected':'' }>Empty</option>
														<option value="not empty" ${query.idCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
														<option value="null or empty" ${query.idCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
														<option value="not (null or empty)" ${query.idCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
													</select><input name="id" value="${query.id }"/>
												</td>
												<td>
													英文名称:<select name="enameCompare">
														<option value="=" ${query.enameCompare eq '='?'selected':'' }>=</option>
														<option value="!=" ${query.enameCompare eq '!='?'selected':'' }>!=</option>
														<option value="&lt;" ${query.enameCompare eq '>'?'selected':'' }>&lt;</option>
														<option value="&lt;=" ${query.enameCompare eq '>='?'selected':'' }>&lt;=</option>
														<option value="&gt;" ${query.enameCompare eq '<'?'selected':'' }>&gt;</option>
														<option value="&gt;=" ${query.enameCompare eq '<='?'selected':'' }>&gt;=</option>
														<option value="in" ${query.enameCompare eq 'in'?'selected':'' }>In</option>
														<option value="not in" ${query.enameCompare eq 'not in'?'selected':'' }>NotIn</option>
														<option value="like" ${query.enameCompare eq 'like'?'selected':'' }>Like</option>
														<option value="not like" ${query.enameCompare eq 'not like'?'selected':'' }>NotLike</option>
														<option value="left like" ${query.enameCompare eq 'left like'?'selected':'' }>LeftLike</option>
														<option value="right like" ${query.enameCompare eq 'right like'?'selected':'' }>RightLike</option>
														<option value="is null" ${query.enameCompare eq 'is null'?'selected':'' }>Null</option>
														<option value="is not null" ${query.enameCompare eq 'is not null'?'selected':'' }>NotNull</option>
														<option value="empty" ${query.enameCompare eq 'empty'?'selected':'' }>Empty</option>
														<option value="not empty" ${query.enameCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
														<option value="null or empty" ${query.enameCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
														<option value="not (null or empty)" ${query.enameCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
													</select><input name="ename" value="${query.ename }"/>
												</td>
												<td>
													中文名称:<select name="cnameCompare">
														<option value="=" ${query.cnameCompare eq '='?'selected':'' }>=</option>
														<option value="!=" ${query.cnameCompare eq '!='?'selected':'' }>!=</option>
														<option value="&lt;" ${query.cnameCompare eq '>'?'selected':'' }>&lt;</option>
														<option value="&lt;=" ${query.cnameCompare eq '>='?'selected':'' }>&lt;=</option>
														<option value="&gt;" ${query.cnameCompare eq '<'?'selected':'' }>&gt;</option>
														<option value="&gt;=" ${query.cnameCompare eq '<='?'selected':'' }>&gt;=</option>
														<option value="in" ${query.cnameCompare eq 'in'?'selected':'' }>In</option>
														<option value="not in" ${query.cnameCompare eq 'not in'?'selected':'' }>NotIn</option>
														<option value="like" ${query.cnameCompare eq 'like'?'selected':'' }>Like</option>
														<option value="not like" ${query.cnameCompare eq 'not like'?'selected':'' }>NotLike</option>
														<option value="left like" ${query.cnameCompare eq 'left like'?'selected':'' }>LeftLike</option>
														<option value="right like" ${query.cnameCompare eq 'right like'?'selected':'' }>RightLike</option>
														<option value="is null" ${query.cnameCompare eq 'is null'?'selected':'' }>Null</option>
														<option value="is not null" ${query.cnameCompare eq 'is not null'?'selected':'' }>NotNull</option>
														<option value="empty" ${query.cnameCompare eq 'empty'?'selected':'' }>Empty</option>
														<option value="not empty" ${query.cnameCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
														<option value="null or empty" ${query.cnameCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
														<option value="not (null or empty)" ${query.cnameCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
													</select><input name="cname" value="${query.cname }"/>
												</td>
												<td><button>查询</button></td>
											</tr>
										</tbody>
									</table>
								</form>
								<%-- <table class="ui-operate-top">
									<tbody>
										<tr>
											<td>
												<a href="javascript:select();" class="button small green">全选</a>
												<a href="javascript:inverse();" class="button small green">反选</a>
												<a href="javascript:operate({url:'${ctx }auth/tree/role/plus.do',target:'_blank'});" class="button small green">批量添加</a>
											</td>
											<td>
												<input type="button" value="高级查询">
												<button form="${not empty applicationScope.mvc.queryForm ? '' : 'queryForm' }">查询</button>
												<input type="button" value="↓" title="展开所有查询条件">
												<input type="button" value="↑" style="display: none;">
												<input type="button" value="字段" title="字段的选择和排序">
											</td>
										</tr>
									</tbody>
								</table> --%>
								<table class="ui-data">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>主键</th>
											<th>英文名称</th>
											<th>中文名称</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${empty validateNodeResult.datas }">
											<tr><td colspan="5">暂无</td></tr>
										</c:if>
										<c:forEach var="item" items="${validateNodeResult.datas }">
											<tr>
												<td><input type="checkbox"/></td>
												<td>${item.id }</td>
												<td>${item.ename }</td>
												<td>${item.cname }</td>
												<td>
													<a href="javascript:jump('${ctx }auth/tree/rolefunc/plus.do','id=${currentNode.id }','resId=${item.id }');"><img src="${ctx }img/operate/add.gif">[添加]</a>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
								<!-- <table class="ui-operate-middle">
									<tbody>
										<tr><td>
											<a href="javascript:select();" class="button small green">全选</a>
											<a href="javascript:inverse();" class="button small green">反选</a>
										</td></tr>
									</tbody>
								</table> -->
								<table class="ui-operate-bottom">
									<tbody>
										<tr>
											<td>
												<jsp:include page="/WEB-INF/component/page.jsp">
													<jsp:param value="${validateNodeResult.page }" name="page"/>
												</jsp:include>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th></th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th></th>
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>