<%@ page pageEncoding="UTF-8"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/add2.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/link/link.js" type="text/javascript"></script>
<table class="ui-list">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th><img src="${ctx }img/operate/table.gif">查看用户</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td>
				<form action="">
					<table class="ui-add">
						<tbody>
							<tr>
								<td>名称:</td>
								<td>${look.name }</td>
							</tr>
							<tr>
								<td>密码:</td>
								<td>${look.password }</td>
							</tr>
							<tr>
								<td>状态:</td>
								<td>
									${look.state eq '1' ? '启用' : '' }
									${look.state eq '0' ? '禁用' : '' }
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<input type="button" value="返回" onclick="back()"/>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>

<script>
	$(document).ready(function() { $(".ui-add").colorize(); });
</script>