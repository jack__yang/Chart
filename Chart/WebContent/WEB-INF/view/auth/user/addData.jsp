<%@ page pageEncoding="UTF-8"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/add2.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/jquery.md5/jQuery.md5.js" type="text/javascript"></script>
<script src="${ctx }js/auth/user/add.js" type="text/javascript"></script>
<table class="ui-list">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th><img src="${ctx }img/operate/table.gif">新增用户</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td>
				<form id="addForm" method="post" action="${ctx }auth/user/add.do">
					<table class="ui-add">
						<tbody>
							<tr>
								<td>名称:</td>
								<td><input name="name"/></td>
							</tr>
							<tr>
								<td>密码:</td>
								<td><input id="password" name="password" type="password"/></td>
							</tr>
							<tr>
								<td>状态:</td>
								<td>
									<select name="state">
										<option value="1">启用</option>
										<option value="0">禁用</option>
									</select>
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<input type="button" value="新增" onclick="add()"/>
									<input type="reset" value="重置"/>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>

<script>
	$(document).ready(function() { $(".ui-add").colorize(); });
</script>