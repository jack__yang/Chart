<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/query.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/operate.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/data.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/link/link.js" type="text/javascript"></script>
<table class="ui-list">
	<thead>
		<tr>
			<th></th>
			<th><img src="${ctx }img/operate/table.gif">资源类型列表</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td>
				<form method="post" action="${ctx }auth/restype/list.do">
					<table class="ui-query">
						<tbody>
							<tr>
								<td>
									<%-- 属性名/比较符/值，三者之间不要有空白字符 --%>
									主键:<select name="idCompare">
										<option value="=" ${query.idCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.idCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.idCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.idCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.idCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.idCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.idCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.idCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.idCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.idCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.idCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.idCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.idCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.idCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.idCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.idCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.idCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.idCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="id" value="${query.id }"/>
								</td>
								<td>
									英文名称:<select name="enameCompare">
										<option value="=" ${query.enameCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.enameCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.enameCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.enameCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.enameCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.enameCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.enameCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.enameCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.enameCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.enameCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.enameCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.enameCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.enameCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.enameCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.enameCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.enameCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.enameCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.enameCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="ename" value="${query.ename }"/>
								</td>
								<td>
									中文名称:<select name="cnameCompare">
										<option value="=" ${query.cnameCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.cnameCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.cnameCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.cnameCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.cnameCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.cnameCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.cnameCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.cnameCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.cnameCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.cnameCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.cnameCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.cnameCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.cnameCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.cnameCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.cnameCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.cnameCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.cnameCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.cnameCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="cname" value="${query.cname }"/>
								</td>
								<td>
									编号:<select name="valueCompare">
										<option value="=" ${query.valueCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.valueCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.valueCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.valueCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.valueCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.valueCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.valueCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.valueCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.valueCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.valueCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.valueCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.valueCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.valueCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.valueCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.valueCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.valueCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.valueCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.valueCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="value" value="${query.value }"/>
								</td>
							</tr>
							<tr>
								<td>
									排序:<select name="sortCompare">
										<option value="=" ${query.sortCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.sortCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.sortCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.sortCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.sortCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.sortCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.sortCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.sortCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.sortCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.sortCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.sortCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.sortCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.sortCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.sortCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.sortCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.sortCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.sortCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.sortCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="sort" value="${query.sort }"/>
								</td>
								<td>
									备注:<select name="remarkCompare">
										<option value="=" ${query.remarkCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.remarkCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.remarkCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.remarkCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.remarkCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.remarkCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.remarkCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.remarkCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.remarkCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.remarkCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.remarkCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.remarkCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.remarkCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.remarkCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.remarkCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.remarkCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.remarkCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.remarkCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="remark" value="${query.remark }"/>
								</td>
								<td>&nbsp;</td>
								<td><button>查询</button></td>
							</tr>
						</tbody>
					</table>
				</form>
				<table class="ui-operate-top">
					<tbody>
						<tr>
							<td>
								<a class="button small green">全选</a>
								<a class="button small green">反选</a>
								<a class="button small green">批量禁用</a>
								<a class="button small green">批量删除</a>
							</td>
							<td>
								<a href="${ctx }auth/restype/toadd.do"><img src="${ctx }img/operate/add.gif">[导出]</a>
								<a href="${ctx }auth/restype/toadd.do"><img src="${ctx }img/operate/add.gif">[导入]</a>
								<a href="${ctx }auth/restype/toadd.do"><img src="${ctx }img/operate/add.gif">[新增]</a>
							</td>
						</tr>
					</tbody>
				</table>
				<table class="ui-data">
					<thead>
						<tr>
							<th><input type="checkbox"/></th>
							<th>主键</th>
							<th>英文名称</th>
							<th>中文名称</th>
							<th>编号</th>
							<th>排序</th>
							<th>备注</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${pageResult.datas }">
							<tr>
								<td><input type="checkbox"/></td>
								<td>${item.id }</td>
								<td>${item.ename }</td>
								<td>${item.cname }</td>
								<td>${item.value }</td>
								<td>${item.sort }</td>
								<td>${item.remark }</td>
								<td>
									<a href="javascript:jump('${ctx }auth/restype/look.do','id=${item.id }');"><img src="${ctx }img/operate/look.gif">[查看]</a>
									<a href="javascript:jump('${ctx }auth/restype/toedit.do','id=${item.id }');"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
									<a href="javascript:jump('${ctx }auth/restype/delete.do','id=${item.id }');"><img src="${ctx }img/operate/delete.gif">[删除]</a>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<table class="ui-operate-middle">
					<tbody>
						<tr><td>
							<a class="button small green">全选</a>
							<a class="button small green">反选</a>
							<a class="button small green">批量禁用</a>
							<a class="button small green">批量删除</a>
						</td></tr>
					</tbody>
				</table>
				<table class="ui-operate-bottom">
					<tbody>
						<tr>
							<td><%@include file="/WEB-INF/component/page.jsp"%></td>
						</tr>
					</tbody>
				</table>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>