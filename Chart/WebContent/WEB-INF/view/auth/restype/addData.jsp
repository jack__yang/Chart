<%@ page pageEncoding="UTF-8"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/add2.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/jquery.md5/jQuery.md5.js" type="text/javascript"></script>
<script src="${ctx }js/auth/restype/add.js" type="text/javascript"></script>
<table class="ui-list">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th><img src="${ctx }img/operate/table.gif">新增资源类型</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td>
				<form id="addForm" method="post" action="${ctx }auth/restype/add.do">
					<table class="ui-add">
						<tbody>
							<tr>
								<td>英文名称:</td>
								<td><input name="ename"/></td>
							</tr>
							<tr>
								<td>中文名称:</td>
								<td><input name="cname"/></td>
							</tr>
							<tr>
								<td>编号:</td>
								<td><input name="value"/></td>
							</tr>
							<tr>
								<td>排序:</td>
								<td><input name="sort"/></td>
							</tr>
							<tr>
								<td>备注:</td>
								<td><textarea rows="5" cols="40" name="remark"></textarea></td>
							</tr>
							<tr>
								<td colspan="4">
									<input type="button" value="新增" onclick="add()"/>
									<input type="reset" value="重置"/>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>

<script>
	$(document).ready(function() { $(".ui-add").colorize(); });
</script>