<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/add2.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/jquery.md5/jQuery.md5.js" type="text/javascript"></script>
<script src="${ctx }js/auth/resource/add.js" type="text/javascript"></script>
<table class="ui-list">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th><img src="${ctx }img/operate/table.gif">新增资源</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td>
				<form id="addForm" method="post" action="${ctx }auth/resource/add.do">
					<table class="ui-add">
						<tbody>
							<tr>
								<td>英文名称:</td>
								<td><input name="ename"/></td>
							</tr>
							<tr>
								<td>中文名称:</td>
								<td><input name="cname"/></td>
							</tr>
							<tr>
								<td>类型:</td>
								<td>
									<select name="type">
										<option value="">请选择</option>
										<c:forEach var="resourceType" items="${resourceTypes }">
											<option value="${resourceType.value }">${resourceType.cname }</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							<tr>
								<td>备注:</td>
								<td><textarea rows="5" cols="40" name="remark"></textarea></td>
							</tr>
							<tr>
								<td colspan="4">
									<input type="button" value="新增" onclick="add()"/>
									<input type="reset" value="重置"/>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>

<script>
	$(document).ready(function() { $(".ui-add").colorize(); });
</script>