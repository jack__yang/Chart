<%@ page pageEncoding="UTF-8"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/add2.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/jquery.md5/jQuery.md5.js" type="text/javascript"></script>
<script src="${ctx }js/auth/function/update.js" type="text/javascript"></script>
<table class="ui-list">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th><img src="${ctx }img/operate/table.gif">编辑用户</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td>
				<form id="updateForm" method="post" action="${ctx }auth/function/edit.do">
					<table class="ui-add">
						<tbody>
							<tr>
								<td>英文名称:</td>
								<td><input name="ename" value="${update.ename }"/></td>
							</tr>
							<tr>
								<td>中文名称:</td>
								<td><input name="cname" value="${update.cname }"/></td>
							</tr>
							<tr>
								<td>备注:</td>
								<td><textarea rows="5" cols="40" name="remark">${update.remark }</textarea></td>
							</tr>
							<tr>
								<td colspan="4">
									<input name="id" value="${update.id }" type="hidden"/>
									<input type="button" value="修改" onclick="update()"/>
									<input type="reset" value="重置"/>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>

<script>
	$(document).ready(function() { $(".ui-add").colorize(); });
</script>