$(document).ready(function() {
	resizeHeight();
	window.onresize = resizeHeight;
	function resizeHeight(){
		var windowHeight = $(window).height();
		var footer = $("#ui-layout-footer");
		var position = parseInt(isNaN(footer.offset().top)?0:footer.offset().top) + parseInt(isNaN(footer.height())?0:footer.height());
		if(position < windowHeight){
			$("#ui-layout-space").height(windowHeight - position - 1);
		} else {
			$("#ui-layout-space").height(0);
		}
	}
});