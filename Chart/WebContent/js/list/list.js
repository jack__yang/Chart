/**
 * 参数
 */
var param = {
	checkbox:"input[type='checkbox'][name='checkbox']",
	selector:"input[type='checkbox'][name='checkbox']:checked",
	listForm:".ui-list>thead>tr>th>.list-form",
	dataName:"data-json",
	target:"_self"
};

/**
 * 全选
 */
function select(){
	$(param.checkbox).attr("checked","true");
}

/**
 * 反选
 */
function inverse(){
	$(param.checkbox).each(function(){
		var checkbox=$(this);
		if(checkbox.attr("checked")=="checked"){
			checkbox.removeAttr("checked");
		} else {
			checkbox.attr("checked","true");
		}
	});
}

/**
 * 批量操作
 * @param option
 */
function operate(option){
	$.extend(param,option);
	batch(param);
}

/**
 * 批量查看
 * @param option
 */
function look(option){
	$.extend(param,option);
	batch(param);
}

/**
 * 批量编辑
 * @param option
 */
function edit(option){
	$.extend(param,option);
	batch(param);
}

/**
 * 批量启用
 * @param option
 */
function open(option){
	$.extend(param,option);
	batch(param);
}

/**
 * 批量禁用
 * @param option
 */
function close(option){
	$.extend(param,option);
	batch(param);
}

/**
 * 批量反用
 * @param option
 */
function clopen(option){
	$.extend(param,option);
	batch(param);
}

/**
 * 批量删除
 * @param option
 */
function deletes(option){
	$.extend(param,option);
	batch(param);
}

/**
 * 选择导出
 * @param option
 */
function exportChoose(option){
	$.extend(param,option);
	var objects=$(param.selector);
	if(objects.length<1){
		alert("请勾选需要批量操作的数据。");
		return ;
	}
	var form = $(param.listForm);
	form.empty();
	objects.each(function(){
		// 执行每个对象
		var checkbox=$(this);
		var json = eval("("+checkbox.attr(param.dataName)+")");
		if(json.id){
			// 主键不为空
			form.append($('<input type="hidden" name="ids" value="'+json.id+'">'));
		}
	});
	form.attr("action",param.url);
	form.attr("target",param.target);
	form.attr("method","post");
	form.submit();
}

/**
 * 导出数据
 * @param option
 */
function exportOption(option){
	$.extend(param,option);
	var form = $(param.queryForm);
	var action = form.attr("action");
	var target = form.attr("target");
	var method = form.attr("method");
	form.attr("action",param.url);
	form.attr("target",param.target);
	form.attr("method","post");
	form.submit();
	form.attr("action",action);
	form.attr("target",target);
	form.attr("method",method);
}

/**
 * 新增导入
 * @param option
 */
function importAdd(option){
	$.extend(param,option);
	var form = $(param.listForm);
	form.empty();
	var action = form.attr("action");
	var target = form.attr("target");
	var method = form.attr("method");
	var enctype = form.attr("method");
	form.attr("action",param.url);
	form.attr("target",param.target);
	form.attr("method","post");
	form.attr("enctype","multipart/form-data");
	var file=$('<input type="file" name="file" onchange="handleFiles(this.files)"/>');
	form.append(file);
	$.extend(param,{actionFile:action,targetFile:target,methodFile:method,enctypeFile:enctype});
	file.click();
}

/**
 * 编辑导入
 * @param option
 */
function importEdit(option){
	$.extend(param,option);
	var form = $(param.listForm);
	form.empty();
	var action = form.attr("action");
	var target = form.attr("target");
	var method = form.attr("method");
	var enctype = form.attr("method");
	form.attr("action",param.url);
	form.attr("target",param.target);
	form.attr("method","post");
	form.attr("enctype","multipart/form-data");
	var file=$('<input type="file" name="file" onchange="handleFiles(this.files)"/>');
	form.append(file);
	$.extend(param,{actionFile:action,targetFile:target,methodFile:method,enctypeFile:enctype});
	file.click();
}

/**
 * 新增或编辑
 * @param option
 */
function importAddOrEdit(option){
	$.extend(param,option);
	var form = $(param.listForm);
	form.empty();
	var action = form.attr("action");
	var target = form.attr("target");
	var method = form.attr("method");
	var enctype = form.attr("method");
	form.attr("action",param.url);
	form.attr("target",param.target);
	form.attr("method","post");
	form.attr("enctype","multipart/form-data");
	var file=$('<input type="file" name="file" onchange="handleFiles(this.files)"/>');
	form.append(file);
	$.extend(param,{actionFile:action,targetFile:target,methodFile:method,enctypeFile:enctype});
	file.click();
}

/**
 * 文件变化处理方法
 * @param files
 */
function handleFiles(files){
	var form = $(param.listForm);
	form.submit();
	form.attr("action",param.actionFile);
	form.attr("target",param.targetFile);
	form.attr("method",param.methodFile);
	form.attr("enctype",param.enctypeFile);
}

/**
 * 批处理
 * @param option
 */
function batch(option){
	// 选中对象
	var objects=$(option.selector);
	if(objects.length<1){
		alert("请勾选需要批量操作的数据。");
		return ;
	}
	objects.each(function(){
		// 执行每个对象
		var checkbox=$(this);
		var json = eval("("+checkbox.attr(option.dataName)+")");
		if(json && json.param){
			// 主键不为空
			var form = $(option.listForm);
			form.empty();
			for(var i = 0; i < json.param.length; i++){
				form.append($('<input type="hidden" name="'+json.param[i]+'" value="'+json[json.param[i]]+'">'));
			}
			form.attr("action",option.url);
			form.attr("target",option.target);
			form.attr("method","post");
			form.submit();
		}
	});
}

/**
 * 返回
 */
function back(){
	window.history.back();
}