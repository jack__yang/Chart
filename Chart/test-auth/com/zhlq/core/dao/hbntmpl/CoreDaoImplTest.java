package com.zhlq.core.dao.hbntmpl;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.zhlq.auth.user.bean.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:com/zhlq/test/config/spring/applicationContext-dataSource.xml",
	"classpath:com/zhlq/test/config/spring/applicationContext-sessionFactory.xml",
	"classpath:com/zhlq/test/config/spring/applicationContext-core.xml",
	"classpath:com/zhlq/test/config/spring/applicationContext-transaction.xml",
	"classpath:com/zhlq/test/config/spring/applicationContext-tree.xml"
})
public class CoreDaoImplTest {
	
	@Autowired
	@Qualifier("coreDaoHt")
	CoreDao coreDao;

	@Before
	public void setUp() throws Exception {
//		String table = "auth_user";
//		File file = new File(PathUtil.getJavaProjectPath()+"test"+File.separator+table+".xml");
//		String path = PathUtil.getFixedPath()+"WEB-INF/config/jdbc/JdbcConfigMysql.properties";
//		System.out.println(path);
//		JdbcUtil.load(path);
//		DbUnitUtil.export(file, JdbcUtil.getConnection(), table);
//		String[] contextPath = {
//				PathUtil.getFixedPath()+"WEB-INF/config/spring/applicationContext-dataSource.xml",
//				PathUtil.getFixedPath()+"WEB-INF/config/spring/applicationContext-sessionFactory.xml",
//				PathUtil.getFixedPath()+"WEB-INF/config/spring/applicationContext-core.xml",
//				PathUtil.getFixedPath()+"WEB-INF/config/spring/applicationContext-transaction.xml",
//				PathUtil.getFixedPath()+"WEB-INF/config/spring/applicationContext-tree.xml"};
//		coreDao = (CoreDao) SpringContextUtil.getBean("coreDao",contextPath);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreate() {
		User user = new User();
		user.setName("Test");
		user.setPassword("TestPassword");
		user.setState("1");
		coreDao.create(user);
	}

	@Test
	public void testRetrieveObject() {
		fail("Not yet implemented");
	}

	@Test
	public void testRetrieveWhere() {
		fail("Not yet implemented");
	}

	@Test
	public void testRetrieveWherePage() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteObject() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteCollectionOfQ() {
		fail("Not yet implemented");
	}

	@Test
	public void testCount() {
		fail("Not yet implemented");
	}

	@Test
	public void testCountWhere() {
		fail("Not yet implemented");
	}

}
