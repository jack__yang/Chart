/**
 * @Title RoleCtrlTest.java
 * @Package com.zhlq.auth.role.ctrl.anotation
 * @Description TODO
 * @author ZHLQ
 * @date 2015年4月18日 下午8:12:28
 * @version V1.0
 */
package com.zhlq.auth.role.ctrl.annotation;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @ClassName RoleCtrlTest
 * @Description TODO
 * @author ZHLQ
 * @date 2015年4月18日 下午8:12:28
 */
public class RoleCtrlTest {

	/**
	 * setUpBeforeClass(这里用一句话描述这个方法的作用)
	 * TODO(这里描述这个方法适用条件 – 可选)
	 * TODO(这里描述这个方法的执行流程 – 可选)
	 * TODO(这里描述这个方法的使用方法 – 可选)
	 * TODO(这里描述这个方法的注意事项 – 可选)
	 *
	 * @Title setUpBeforeClass
	 * @Description TODO
	 * @param @throws java.lang.Exception 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * tearDownAfterClass(这里用一句话描述这个方法的作用)
	 * TODO(这里描述这个方法适用条件 – 可选)
	 * TODO(这里描述这个方法的执行流程 – 可选)
	 * TODO(这里描述这个方法的使用方法 – 可选)
	 * TODO(这里描述这个方法的注意事项 – 可选)
	 *
	 * @Title tearDownAfterClass
	 * @Description TODO
	 * @param @throws java.lang.Exception 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * setUp(这里用一句话描述这个方法的作用)
	 * TODO(这里描述这个方法适用条件 – 可选)
	 * TODO(这里描述这个方法的执行流程 – 可选)
	 * TODO(这里描述这个方法的使用方法 – 可选)
	 * TODO(这里描述这个方法的注意事项 – 可选)
	 *
	 * @Title setUp
	 * @Description TODO
	 * @param @throws java.lang.Exception 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * tearDown(这里用一句话描述这个方法的作用)
	 * TODO(这里描述这个方法适用条件 – 可选)
	 * TODO(这里描述这个方法的执行流程 – 可选)
	 * TODO(这里描述这个方法的使用方法 – 可选)
	 * TODO(这里描述这个方法的注意事项 – 可选)
	 *
	 * @Title tearDown
	 * @Description TODO
	 * @param @throws java.lang.Exception 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.zhlq.auth.role.ctrl.anotation.RoleCtrl#list(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.zhlq.page.Page, com.zhlq.auth.role.bean.RoleForm)}.
	 */
	@Test
	public void testList() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.zhlq.auth.role.ctrl.anotation.RoleCtrl#toAdd(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}.
	 */
	@Test
	public void testToAdd() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.zhlq.auth.role.ctrl.anotation.RoleCtrl#add(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.zhlq.auth.role.bean.Role)}.
	 */
	@Test
	public void testAdd() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.zhlq.auth.role.ctrl.anotation.RoleCtrl#delete(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.zhlq.auth.role.bean.Role)}.
	 */
	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.zhlq.auth.role.ctrl.anotation.RoleCtrl#look(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.zhlq.auth.role.bean.Role)}.
	 */
	@Test
	public void testLook() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.zhlq.auth.role.ctrl.anotation.RoleCtrl#toEdit(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.zhlq.auth.role.bean.Role)}.
	 */
	@Test
	public void testToEdit() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.zhlq.auth.role.ctrl.anotation.RoleCtrl#edit(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.zhlq.auth.role.bean.Role)}.
	 */
	@Test
	public void testEdit() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.zhlq.auth.role.ctrl.anotation.RoleCtrl#open(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.zhlq.auth.role.bean.Role)}.
	 */
	@Test
	public void testOpen() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.zhlq.auth.role.ctrl.anotation.RoleCtrl#close(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.zhlq.auth.role.bean.Role)}.
	 */
	@Test
	public void testClose() {
		fail("Not yet implemented");
	}

}
