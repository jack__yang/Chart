package com.zhlq.auth.user.ctrl.annotation;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebResponse;

//@RunWith(TestClassRunner.class)
//@RunWith(Parameterized.class)
//@RunWith(Suite.class)
//@SuiteClasses({ UserCtrlTest.class })
public class UserCtrlTest {

	@Test(expected = IOException.class)
	public void testList() throws IOException, SAXException {
		String urlString = "http://127.0.0.1:8080/Auth/";
		WebConversation webConversation = new WebConversation();
		HttpUnitOptions.setScriptingEnabled(false);
		WebResponse webResponse = webConversation.getResponse(urlString);
		System.out.println(webResponse.getText());
	}

}
