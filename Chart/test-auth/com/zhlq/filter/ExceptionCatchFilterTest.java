package com.zhlq.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ExceptionCatchFilterTest {

	/**
	 * 异常页面
	 */
	@RequestMapping("exception")
	public String list(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("begin:http://localhost:8080/Auth/exception.do");
		throw new RuntimeException();
//		return "exctption";
	}

}
